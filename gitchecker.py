#!/usr/bin/python
from win10toast import ToastNotifier
import datetime
import os
import subprocess
import sys
from time import sleep

def main():
    toaster = ToastNotifier()
    print("")
    subprocess.call(["git", "fetch"])
    out = subprocess.check_output(["git", "diff", "--name-status", "origin/master"])
    if out:
        print(datetime.datetime.now())
        print("===================================")
        print(out.decode(), end='')
        print("===================================")
        toaster.show_toast("New changes!",
            str(out.decode()),
            icon_path=local + "/flag.ico",
            duration=10)
    sleep(90)
    main()

if __name__ == "__main__":
    local = os.path.dirname(__file__)
    if (len(sys.argv) > 1):
        os.chdir(sys.argv[1])
    try:
        main()
    except KeyboardInterrupt:
        print("Stopped")