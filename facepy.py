#!/usr/bin/python
import urllib.request
from threading import Thread

def main():
    with open('response.js') as f:
        lines = f.readlines()
    hovers = lines[0].split('friend_browser_id')
    user_ids = []
    for h in hovers:
        if "]=" not in h:
            continue
        php_split = h.split("]=")[1]
        if len(php_split.split("&")) > 1:
            user_ids.append(php_split.split("&")[0])
        else:
            user_ids.append(php_split.split("\\\"")[0])
    threads = []
    list(map(lambda p: threads.append(Thread(target=download_picture, args=([p]))), user_ids))
    list(map(lambda t: t.start(), threads))


def download_picture(user_id):
    graph_url = 'http://graph.facebook.com/%s/picture?type=square&height=500'
    urllib.request.urlretrieve(graph_url % user_id, "pics/" + user_id + ".png")
    print('Downloaded: ' + user_id)


if __name__ == "__main__":
    main()
