#!/bin/bash
while getopts ":f:o:" p; do
  case "${p}" in
    f)
      infile=${OPTARG}
    ;;
    o)
      outfile=${OPTARG}
    ;;
  esac
done

tested=\;
noscan=("build_ambiente" "app_name")

rdom () {
    local IFS=\>
    read -d \< E C
    local ret=$?
    TAG_NAME=${E%% *}
    ATTRIBUTES=${E#*}
    return $ret
}

parse_dom() {
  eval local $ATTRIBUTES
  if [[ $name != "" ]] && [[ ! $noscan[@] =~ $name ]]; then
    if [[ $1 == "" ]]; then
      if [[ $tested != *\;"$C"\;* ]]; then
        tested+="$C"\;
        iterate_dom "$C"
      fi
    elif [[ $1 == "$C" ]]; then
      those[$count]=$(echo $name "=" \"$C\")
      (( count++ )) 
    fi
  fi
}

iterate_dom() {
  count=0
  unset those
  while rdom; do
    parse_dom "$1"
  done < $infile 2>/dev/null

  if [[ ${#those[@]} > 1 ]]; then
    printf '%s\n' "${those[@]}" | tee -a $outfile
    echo | tee -a $outfile & echo | tee -a $outfile & echo | tee -a $outfile
  fi
}

if [[ $outfile == "" ]]; then
  echo "-f xml -o out"
  exit 1
fi

if [[ $infile == *.xml ]]; then
  echo > $outfile
  iterate_dom
fi
