#!/usr/local/bin/python

from random import Random

def init():
    overall = []
    games = 10
    for x in range(0, games * 10):
        score = games
        for z in range(0, score):
            score = score - 1 if not new_round() else score
        overall.append(score)
    print(str(sum(overall) / len(overall)) + " out of " + str(games))

def new_round():
    # Place the prize in one of the doors and get its position
    doors = place_prize(r)
    prize_pos = doors.index(True)
    # The guest randomly chooses a door
    chosen_pos = doors[r.randint(0, 2)]
    #print(chosen_pos)
    # Monty opens a door that does not has the prize, and is not the chosen one
    for x in range(0, 3):
        open_door = False if x == chosen_pos or x == prize_pos else True
        if open_door:
             open_door_pos = x
             break
    # Monty offers the switch door option
    will_switch = True
    # If the guest decides to switch...
    for x in range(0, 3):
        if will_switch and (x != chosen_pos and x != open_door_pos):
            chosen_pos = x
            break
    # Now we see if the guest wins or looses
    #print("Winner!" if chosen_pos == prize_pos else "Loser!")
    return True if chosen_pos == prize_pos else False

def place_prize(randomInstance):
    pos = randomInstance.randint(0, 2)
    return (pos == 0, pos == 1, pos == 2)

r = Random()

if __name__ == "__main__":
    init()