#!/bin/bash
while getopts ":p:m:i:" o; do
  case "${o}" in
    i)
      install=${OPTARG}
    ;;
    p)
      projDir=~/AndroidStudioProjects/${OPTARG}
    ;;
    m)
      moduleName=${OPTARG}
    ;;
  esac
done

adb=adb

if [ "${projDir}" == "" ]; then
  echo [!] Missing project name
  exit 1
fi

if [ -d ${projDir} ]; then
  cd /
  devices=$($adb devices | grep -w "device")
  if [[ ${install} != "noinstall" ]]; then
    if [[ "$devices" == "" ]]; then
      echo [!] Connect a device and try again
      exit 1
    fi
  fi
  cd ${projDir}
  echo [+] Building ${projDir}...
  apk="${projDir}/app/build/outputs/apk/app-${moduleName,,}.apk"
  if [[ -f $apk ]]; then
    echo [+] Deleted old file
    rm $apk
  fi
   ./gradlew assemble${moduleName}
  if [[ $? == 0 ]]; then
    clear
    cd /
    echo [+] Build successful
    if [[ $install == "noinstall" ]]; then
      apk="${projDir}/app/build/outputs/apk/app-${moduleName,,}.apk"
    else
      echo [+] Installing on device...
      $adb install -r "${projDir}/app/build/outputs/apk/app-${moduleName,,}.apk"
      echo [+] Installation successful
      exit 0
    fi
  else
    echo [!] Error on build stage
    exit 1
  fi
else
  echo [!] Project not found
  exit 1
fi
