#!/bin/bash

while getopts ":z:p:" o; do
  case "$o" in
    z)
      zipsPath=$OPTARG
    ;;
    p)
      projResPath=$OPTARG/app/src/main/res
    ;;
  esac
done

tmpPath="$(pwd)"/upkenovTmp

for f in $zipsPath/ic_*.zip; do
  unzip -o -qq $f -d $tmpPath
done

for d in $tmpPath/res/drawable-*; do
  size="${d##*-}"
  mv $tmpPath/res/drawable-$size/* $projResPath/mipmap-$size/
done

rm -rf $tmpPath
