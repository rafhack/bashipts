#!/usr/bin/python
import configparser
import json
import os
import subprocess
import sys

from colorama import init

class Color:
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    END = '\033[0m'


def search_menu():
    string = ""
    while not string:
        print(Color.YELLOW + "\nEnter the string to search: " + Color.END, end="")
        string = input()

    os.chdir("translation")
    strfile = 'iOS_smart_English_US.json'

    f = open(strfile, "r", encoding="utf8")
    data = json.loads(f.read())
    f.close()

    results = []
    
    for i, line in enumerate(data):
        if string.lower() in line.lower():
            results.append(line + "~" + str(i))

    if not results:
        print("String " + Color.RED + Color.BOLD + string + Color.END + " not found")
        os.chdir("..")
        os.chdir("..")
        input()
        return
    columns = os.get_terminal_size().columns
    for i, l in enumerate(results):
        print(Color.RED + Color.BOLD + "[" + str(i + 1) + "] " + Color.GREEN, end='')    
        result = l.split("~")[0][:columns-(7 + len(str(i + 1)) - 1)]
        uprint(result + Color.CYAN + ("..." if len(result) + 7 + len(str(i + 1)) - 1 >= columns else ""))

    print(Color.RED + Color.BOLD + "[0]" + Color.YELLOW + "  Back")

    choice = ""
    while not choice or not (-1 < int(choice) <= len(results)):
        print(Color.END + "Enter your choice: ", end="")
        choice = input()

    if int(choice) == 0:
        os.chdir("..")
        os.chdir("..")
        return

    new_key = ""
    while not new_key:
        print("Enter the new key: its4_", end="")
        new_key = input()

    write_new_strings("its4_" + new_key, results[int(choice) - 1])


def write_new_strings(new_key, json_key):

    str_dirs = {
        "/values/": "iOS_smart_English_US.json",
        "/values-nl/": "iOS_smart_Dutch.json",
        "/values-it/": "iOS_smart_Italian.json",
        "/values-pt/": "iOS_smart_Portuguese_BR.json"
    }

    print("\n")
    for k, v in str_dirs.items():
        new_str = get_str_for_key(path + "/ginter/translation/" + v, json_key.split('~')[0])

        if type(new_str) is dict:
            for dk, dv in new_str.items():
                new_str = dv
                break

        new_str = json_key.split('~')[0] + "" if len(new_str) == 0 else new_str

        if contains_in_file(dest_res + k + "strings.xml", '>{0}<'.format(new_str.replace(
                '\n', '').replace('"', '').strip())):
            print(Color.RED + Color.BOLD, end='')
            print("String %s already exists in %s" % ((new_str.replace('\n', '').replace('"', '').strip()), k), end='')
            print("... Adding anyway" + Color.END)
        if contains_in_file(dest_res + k + "strings.xml", '"{0}"'.format(new_key)):
            print(Color.RED + Color.BOLD + "Key already exists" + Color.END)
            break
        create_string_in_file(dest_res + k + "strings.xml", new_str, new_key)
    print("\n")
    print_info("Process completed")
    input()
    return


def contains_in_file(file, text):
    if text in open(file).read():
        return True


def get_str_for_key(file, key):
    sf = open(file, "r", encoding="utf8")
    lines = json.loads(sf.read())
    sf.close()
    return lines[key]


def create_string_in_file(string_file, content, newkey):
    strfile = open(string_file, "r+", encoding="utf-8")
    lines = strfile.readlines()

    splited = str(content).split("%@")
    new_content = ""
    
    for i, s in enumerate(splited):
        ph = "%" + str(i+1) + "$s" if i < len(splited) - 1 else ""
        new_content += s + ph

    lines.insert(len(lines) - 1,
                 "    <string name=\"" + newkey + "\">" +
                 str(new_content).replace('"', '').replace("\'", "\\'").strip() + "</string>\n")
    strfile.truncate(0)
    strfile.seek(0)
    strfile.writelines(lines)
    strfile.close()
    print(Color.GREEN + Color.BOLD + "<string name=\"" + newkey + "\">" + Color.END, end='')
    uprint(str(new_content).replace('"', '').strip(), end='')
    print(Color.GREEN + Color.BOLD + "</string>", end=Color.END + '\n')


def uprint(*objects, sep=' ', end='\n', file=sys.stdout):
    enc = file.encoding
    if enc == 'UTF-8':
        print(*objects, sep=sep, end=end, file=file)
    else:
        f = lambda obj: str(obj).encode(enc, errors='backslashreplace').decode(enc)
        print(*map(f, objects), sep=sep, end=end, file=file)


def check_dest_res():
    global dest_res
    dest_res = ""

    if not (os.path.exists("ginter.conf")):
        while not dest_res or not os.path.isdir(dest_res):
            print(Color.RED + "Please, enter the 'res' directory of your project: " + Color.END, end="")
            dest_res = input()
        write_config('DestRes', "dir", dest_res)

    dest_res = read_config('DestRes', "dir")


def main_menu():
    clear()
    os.chdir(path + "/ginter")
    print_info("Current 'res' directory: " + Color.RED + dest_res)
    print(Color.BOLD + Color.CYAN + "\n\n1 - Set \"res\" destination directory")
    print("2 - Add string to project")
    print("3 - Update repository")
    print("4 - Exit\n" + Color.END)
    choice = ""
    while not choice or not int(choice) in range(1, 5):
        print("> ", end="")
        choice = input()
    if choice == '1':
        os.remove("ginter.conf")
        check_dest_res()
        return
    elif choice == '2':
        search_menu()
    elif choice == '3':
        update_translations()
    elif choice == '4':
        sys.exit(0)


def read_config(section, prop):
    pwd = os.getcwd()
    os.chdir(path)
    os.chdir("ginter")
    cfgfile = open("ginter.conf", "r")
    config = configparser.ConfigParser()
    config.read("ginter.conf")
    cfgfile.close()
    os.chdir(pwd)
    if not config.has_section(section):
        return -1
    return config.get(section, prop)


def write_config(section, prop, value):
    pwd = os.getcwd()
    os.chdir(path)
    os.chdir("ginter")
    cfgfile = open("ginter.conf", 'a')
    config = configparser.ConfigParser()
    config.read("ginter.conf")
    if not (config.has_section(section)):
        config.add_section(section)
    config.set(section, prop, value)
    cfgfile.seek(0)
    cfgfile.truncate()
    config.write(cfgfile)
    cfgfile.close()
    os.chdir(pwd)


def print_info(message):
    print(Color.BOLD + Color.GREEN + "[+] {0}".format(message) + Color.END)


def create_structure():
    if not os.path.isdir("ginter"):
        os.makedirs("ginter")
    os.chdir("ginter")
    if not os.path.isdir("translation"):
        print_info("Cloning project...")
        subprocess.call(["git", "clone", "https://github.com/its4company/translation.git"], shell=False)


def clear():
    if sys.platform == 'win32':
        os.system('cls')
    else:
        os.system('clear')


def update_translations():
    os.chdir("translation")
    print_info("Updating repository")
    subprocess.call(["git", "fetch"])
    out = subprocess.check_output(["git", "diff", "origin/master"])
    if out:
        subprocess.call(["git", "reset", "--hard", "origin/master"])
    print_info("Repository is up-to-date")
    os.chdir("..")


if __name__ == "__main__":
    global path
    path = os.getcwd()
    try:
        init()
        create_structure()
        update_translations()
        check_dest_res()
        while 1:
            try:
                main_menu()
            except ValueError:
                pass
    except KeyboardInterrupt:
        print("\b\b" + Color.BOLD + Color.RED + "Program interrupted!" + Color.END)
        try:
            sys.exit(0)
        except SystemExit:
            # noinspection PyProtectedMember
            os._exit(0)