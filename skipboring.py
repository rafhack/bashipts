#!/usr/bin/python
""" Ok... I hate creating Java classes manually """
from colorama import init

class Color:
    """ Coloring class """
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def main():
    """ Here everything begins """
    opt = menu_maker(
        'Brand new module',
        'Edit existing module',
        'Set project root location'
    )

def set_project_root():
    

def menu_maker(*options):
    """ Creating menus is also boring, so... """
    for i, menu in enumerate(options):
        print(Color.BOLD + "[{0}] {1}".format(i+1, menu) + Color.END)
    opt = 0
    while opt not in range(1, len(options)+1):
        try:
            opt = int(input(Color.GREEN + '> ' + Color.END))
        except ValueError:
            opt = 0
    return opt

if __name__ == "__main__":
    init()
    try:
        main()
    except KeyboardInterrupt:
        print(Color.RED + "\nInterrupt" + Color.END)
