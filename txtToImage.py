from PIL import Image
import math
from random import Random


def newImg(size):
    r = Random()
    img = Image.new('RGB', (size, size))
    i = 0
    for y in range(0, size):
        for x in range(0, size):
            try:
                val = ord(chars[i])
            except IndexError:
                val = 0
            i = i + 1
            grnd = (0 if x * y % 2 == 0 else r.randint(127, 255)) + y - size
            brnd = (0 if y * x % 2 == 0 else r.randint(127, 255)) + y - size
            # print(size / (y + 1))
            img.putpixel((x, y), (val + x, grnd, brnd))
    img.save('sqr.png')


def read(size):
    im = Image.open('sqr.png')
    rgb_im = im.convert('RGB')
    for y in range(0, size):
        for x in range(0, size):
            r, g, b = rgb_im.getpixel((x, y))
            print(chr(r - x), end="")


if __name__ == "__main__":
    file_name = 'scan.py'
    with open(file_name) as f:
        chars = f.read()
        text = chars.splitlines()
        size = len(chars)
        sqr = math.ceil(math.sqrt(size))
        newImg(sqr)
        read(math.ceil(math.sqrt(size)))
# This code turns text into images!
