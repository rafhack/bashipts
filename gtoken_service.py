#!/usr/bin/python3

import os
import sys
import json
import getopt
import datetime

from typing import Dict

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials


def parse_params():
    longopts = ["set=", "get"]
    shoropts = "s:g"
    try:
        opts, _ = getopt.getopt(sys.argv[1:], shoropts, longopts)
        for opt, arg in opts:
            if opt in ('-g', '--get'):
                get_calendar_events(arg=None)
            if opt in ('-s', '--set'):
                get_calendar_events(iCalUID=arg, arg=arg)
        if not opts:
            pass
    except getopt.GetoptError as e:
        print(e)


def get_configuration():
    dir = os.path.dirname(os.path.realpath(__file__))
    rf = open(f"{dir}/stichy-pgn.json", "r")
    configutarionFile = rf.read()
    readConfiguration: Dict = json.loads(configutarionFile)
    rf.close()
    return readConfiguration


def save_configuration(configuration: Dict):
    dir = os.path.dirname(os.path.realpath(__file__))
    wf = open(f"{dir}/stichy-pgn.json", "w")
    wf.write(json.dumps(configuration, sort_keys=True, indent=4))
    wf.close()


def get_google_credentials(arg):
    config = get_configuration()
    scopes = ['https://www.googleapis.com/auth/calendar.readonly']

    dir = os.path.dirname(os.path.realpath(__file__))
    config_file = f"{dir}/stichy-pgn.json"

    try:
        creds = Credentials.from_authorized_user_file(config_file, scopes)
        creds.refresh(Request())
    except:
        flow = InstalledAppFlow.from_client_secrets_file(config_file, scopes)
        flow.redirect_uri = flow._OOB_REDIRECT_URI
        if not arg:
            open_ssh_link(flow.authorization_url()[0])
            return None
        else:
            flow.fetch_token(code=arg)
            creds = flow.credentials
            config.update(json.loads(creds.to_json()))
            save_configuration(config)
            creds = Credentials.from_authorized_user_file(config_file, scopes)

    return creds


def open_ssh_link(url):
    host = '192.168.0.18'
    ssh = f'rafhack@{host}'
    os.system(
        f"ssh {ssh} -t 'psexec \\\\{host} -u rafhack -p 234 -h -s -d -i 1 gtokenUpdate \"{url}\"'")

def open_ssh_event_chooser(events):
    str_arg = map(lambda e: f'{e["summary"]}::{e["iCalUID"]}', events)
    str_arg = ';;'.join(list(str_arg))
    host = '192.168.0.18'
    ssh = f'rafhack@{host}'
    os.system(
        f"ssh {ssh} -t 'psexec \\\\{host} -u rafhack -p 234 -h -s -d -i 1 gcalendarUpdate \"{str_arg}\"'")


def get_calendar_events(iCalUID=get_configuration()["iCalUID"], arg=None):
    creds = get_google_credentials(arg)
    if not creds:
        return
    service = build('calendar', 'v3', credentials=creds)

    now = datetime.datetime.utcnow()
    past_midnight = now - datetime.timedelta(hours=now.hour,
                                             minutes=now.minute, seconds=now.second)
    next_midnight = past_midnight + datetime.timedelta(days=1)
    events_result = service.events().list(calendarId='primary', timeMin=past_midnight.isoformat() + 'Z',
                                          timeMax=next_midnight.isoformat() + 'Z', maxResults=30, singleEvents=True,
                                          orderBy='startTime', iCalUID=iCalUID).execute()
    events = events_result.get('items', [])
    if not iCalUID:
        return events
    try:
        events[0]
        readConfiguration = get_configuration()
        readConfiguration.update({
            "iCalUID": iCalUID
        })
        save_configuration(readConfiguration)
    except IndexError:
        open_ssh_event_chooser(get_calendar_events(None, None))

def set_daily_event(iCalUID):
    c = get_configuration()
    c.update({"iCalUID": iCalUID})
    save_configuration(c)

def init():
    parse_params()


if __name__ == '__main__':
    init()
