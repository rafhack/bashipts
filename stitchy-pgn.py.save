#!/usr/bin/python3
""" Stitch Automator """

import datetime
import getopt
import json
import os
import subprocess
import sys
from typing import Dict, List
from random import random
import time
import requests
import urllib3

SUB_KEY = "Software\HNDSofts\Stitch"


def init():
    parse_params()


def parse_params():
    longopts = ["query", "prepare", "execute=",
                "deactivate" "activate", "configure", "delete"]
    shoropts = "qpe:dacD"
    try:
        opts, _ = getopt.getopt(sys.argv[1:], shoropts, longopts)
        for opt, arg in opts:
            if opt in ('-q', '--query'):
                query_schedule()
            if opt in ('-p', '--prepare'):
                prepare_cron_schedule()
            if opt in ('-a', '--activate'):
                activate_stitch()
            if opt in ('-e', '--execute'):
                execute_stitch(arg)
            if opt in ('-c', '--configure'):
                configure_auth()
            if opt in ('-D', '--delete'):
                delete_schedule()
            if opt in ('-d', '--deactivate'):
                deactivate_schedule()
        if not opts:
            usage()
    except getopt.GetoptError as e:
        print(e)
        usage()


def delete_schedule():
    confirm = input("Delete? [Y/N] ")
    if confirm.lower() != 'y':
        return
    config = get_configuration()
    config.update({
        "crons": [],
        "fromDayOfWeek": "",
        "startTimes": [],
        "toDayOfWeek": ""
    })
    save_configuration(config)
    delete_crons()


def deactivate_schedule():
    confirm = input("Deactivate? [Y/N] ")
    if confirm.lower() == 'y':
        delete_crons()


def delete_crons():
    os.system("crontab -l | grep -v \"#stitch\" | crontab -")


def configure_auth():
    try:
        employer = input("Employer: ")
        pin = input("PIN: ")
        jSessionId = input("JSESSIONID: ")
        userId = input("User ID: ")
        authorization = input("Authorization: ")

        stichyConfiguration = {
            "employer": employer,
            "pin": pin,
            "jSessionId": jSessionId,
            "userId": userId,
            "authorization": authorization
        }

        stichyJsonStr = json.dumps(
            stichyConfiguration, sort_keys=True, indent=4)

        f = open("stichy-pgn.json", "w")
        f.write(stichyJsonStr)
        f.close()

        print("\nConfiguration:\n\n" + stichyJsonStr)
        print("\nNext step: Prepare your schedule by running with \"-p\" flag")

    except OSError as e:
        print(e)


def execute_stitch(arg):
    try:
        print(arg)
        if (arg == "end"):
            stop_clockify()

        #Humanize stitch
	print("waiting")
     time.sleep(get_random_minute())

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        configuration = get_configuration()

        employer = configuration["employer"]
        pin = configuration["pin"]
        user_id = configuration["userId"]
        session_id = configuration["jSessionId"]
        auth = configuration["authorization"]

        baseURL = "https://app.tangerino.com.br/Tangerino/ws"
        headers = {
            "EMPREGADOR": employer,
            "pin": pin,
            "Cookie": "JSESSIONID={}".format(session_id),
            "funcionarioid": user_id,
            "USERNAME": user_id,
            "authorization": auth,
            "Content-Type": "application/json"
        }

        print(requests.get("{}/fingerprintWS/funcionario/empregador/{}/pin/{}"
                           .format(baseURL, employer, pin), verify=False, headers=headers).status_code)

        body = {'deviceId': None}
        print(requests.post("{}/autorizaDipositivoWS/verifica/web/empregador/{}/pin/{}"
                            .format(baseURL, employer, pin), verify=False, headers=headers, data=json.dumps(body)).status_code)

        date_time = datetime.datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        body = {
            "horaInicio": date_time,
            "deviceId": None,
            "online": "true",
            "codigoEmpregador": employer,
            "pin": pin,
            "horaFim": "",
            "tipo": "WEB",
            "foto": "",
            "intervalo": "",
            "validFingerprint": False,
            "versao": "registra-ponto-fingerprint",
            "plataforma": "WEB",
            "funcionarioid": user_id,
            "idAtividade": 6,
            "latitude": None,
            "longitude": None
        }
        print(requests.post("{}/pontoWS/ponto/sincronizacaoPontos/1.2"
                            .format(baseURL), verify=False, headers=headers, data=json.dumps(body)).status_code)

        if (arg == "start"):
            time.sleep(get_random_minute(25, 30))
            start_clockify()

    except Exception as e:
        print("erro")


def activate_stitch():
    try:
        configuration = get_configuration()

        confirm = input("Current schedule: {}\n\nActivate schedule? [Y/N] "
                        .format(configuration))

        if confirm.lower() != 'y':
            return
    
        for i, cron in enumerate(configuration["crons"]):
            print(i)
            pair = i % 2
            action = "start" if pair == 0 else "end"
            command = f"(crontab -l 2>/dev/null; echo \"{cron} {sys.executable} {__file__} -e {action} #stitch\") | crontab -"
            try:
                os.system(command)
            except subprocess.CalledProcessError:
                print("Failed to create schedule")
                return

    except OSError as e:
        print("Could not find schedule with the specified id")
        print(e)


def prepare_cron_schedule():
    start_times = ["09:00", "12:00", "13:00", "18:00"]
    input_times = input(
        "Start Times (Separated by colons, like: \"{}\"): ".format(start_times).replace("[", "").replace("'", "").replace("]", ""))
    start_times = input_times.split(',') if input_times else start_times

    from_day_of_week = input("From day of week (Ex: mon or fri): ")
    from_day_of_week = from_day_of_week if from_day_of_week else "mon"

    to_day_of_week = input("To day of week: ")
    to_day_of_week = to_day_of_week if to_day_of_week else "fri"

    crons = []
    for time in start_times:
        time_units = time.split(":")
        crons.append(
            f"{time_units[1]} {time_units[0]} * * {from_day_of_week.upper()}-{to_day_of_week.upper()}")

    try:
        readConfiguration = get_configuration()
        readConfiguration.update(
            {"startTimes": start_times, "fromDayOfWeek": from_day_of_week, "toDayOfWeek": to_day_of_week, "crons": crons})

        save_configuration(readConfiguration)

    except OSError as e:
        print("Missing configurations. Use -c option")
        print(e)

    query_schedule()


def save_configuration(configuration: Dict):
    wf = open("./stichy-pgn.json", "w")
    wf.write(json.dumps(configuration, sort_keys=True, indent=4))
    wf.close()


def get_configuration():
    dir = os.path.dirname(os.path.realpath(__file__))
    rf = open(f"{dir}/stichy-pgn.json", "r")
    configutarionFile = rf.read()
    readConfiguration: Dict = json.loads(configutarionFile)
    rf.close()
    return readConfiguration


def query_schedule():
    try:
        print(get_configuration()["crons"])
    except OSError as e:
        print(e)


def usage():
    print(
        "Missing arguments\n"
        + f"\n    -q, --query id            Query a specific schedule"
        + f"\n    -q, --query .             Query all schedules\n"
        + f"\n    -p, --prepare             Prepares a new schedule"
        + f"\n                              Formats must be:"
        + f"\n                                  Time - HH:mm"
        + f"\n                                  Date - dd/MM/yyyy"
        + f"\n    -a, --activate id         Activates a prepared schedule"
        + f"\n    -e, --execute action      Executes an activated schedule with action type: \"start\" or \"end\""
        + f"\n    -d, --deactivate id       Deactivates an activated schedule"
        + f"\n    -D, --delete id           Deletes a schedule\n"
        + f"\n    -c, --configure           (Re)configures user information"
    )


def get_random_number(min, max):
    return round(random() * (max - min) + min)


def get_random_minute(minimun_wait=0, maximun_wait=10):
    return get_random_number(minimun_wait, maximun_wait) * 60


def start_clockify():
    try:
        config = get_configuration()

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        baseURL = "https://global.api.clockify.me"
        headers = {
            "x-auth-token": config["clockifyToken"],
            "Content-Type": "application/json"
        }
        date_time = datetime.datetime.today().utcnow().strftime('%Y-%m-%dT%H:%M:%S.000Z')

        body = {
            "start": date_time,
            "billable": False,
            "description": "",
            "projectId": None,
            "taskId": None,
            "tagIds": None,
            "customFields": []
        }
        r = requests.post(f"{baseURL}/workspaces/{config['clockifyWorkspaceId']}/timeEntries/full",
                        verify=False, headers=headers, data=json.dumps(body))

        print(r.status_code)

        config.update({"clockifyTimeEntryId": json.loads(r.text)["id"]})
        save_configuration(config)
    except Exception as e:
        print(e)


def stop_clockify():
    try:
        
        config = get_configuration()

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        baseURL = "https://global.api.clockify.me"
        headers = {
            "x-auth-token": config["clockifyToken"],
            "x-auth-checksum": config["clockifyChecksum"],
            "Content-Type": "application/json"
        }

        body = {
            "projectId": config["clockifyProjectId"]
        }

        r = requests.put(f"{baseURL}/workspaces/{config['clockifyWorkspaceId']}/timeEntries/{config['clockifyTimeEntryId']}/project",
                         verify=False, headers=headers, data=json.dumps(body))

        print(r.status_code)

        response_dict = json.loads(r.text)
        start_time = response_dict['timeInterval']['start']

        body2 = {
            "description": "Bugfixes"
        }

        print(r.status_code)

        r = requests.put(f"{baseURL}/workspaces/{config['clockifyWorkspaceId']}/timeEntries/{config['clockifyTimeEntryId']}/description",
                         verify=False, headers=headers, data=json.dumps(body2))

        print(r.status_code)

        content: Dict = json.loads(r.text)

        date_time = datetime.datetime.today().utcnow().strftime('%Y-%m-%dT%H:%M:%S.000Z')

        body3 = {
            "billable": False,
            "description": content['description'],
            "end": date_time,
            "start": start_time,
            "projectId": content["projectId"],
            "tagIds": None,
            "taskId": None
        }

        url = f"{baseURL}/workspaces/{config['clockifyWorkspaceId']}/timeEntries/{config['clockifyTimeEntryId']}/full"
        r = requests.put(url, verify=False, headers=headers,
                         data=json.dumps(body3))

        print(r.status_code)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    try:
        init()
    except KeyboardInterrupt:
        print("\n\nInterrupted!")
