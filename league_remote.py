from twisted.internet import reactor
from twisted.internet import _threadedselect
from autobahn.twisted.websocket import WebSocketClientFactory, \
    WebSocketClientProtocol, connectWS
from json.decoder import JSONDecodeError
from datetime import datetime
from time import sleep
from threading import Thread
from colorama import init
from datetime import datetime
from riotwatcher import LolWatcher, ApiError
import time
import json
import re
import base64
import requests
import urllib3
import sys
import getopt
import subprocess
import os
import psutil
import ctypes
import ctypes.wintypes

DEBUG = False
session = None
order = 0

pick_action = {}
friends = {}
has_banned = False
arg_champ = None
arg_ban = None
arg_reset = None
arg_mode = None
summonerId = None
pick = None
ban = None
reset_time = None
should_refoucus = False
last_hwnd = None
win_mgr = None
match_accepted = False
has_collected_ids = False
my_region = 'br1'
lol_watcher = LolWatcher('RGAPI-8d604d1b-e130-4d52-b164-910f18fa4efe')

positions = {
    'mid': 'MIDDLE',
    'adc': 'BOTTOM',
    'sup': 'UTILITY',
    'top': 'TOP',
    'jungle': 'JUNGLE',
    'fill': 'FILL'
}

queues = {
    'normal': 400,
    'solo2': 420,
    'blind': 430,
    'flex': 440,
    'aram': 450
}


class LeagueClientProtocol(WebSocketClientProtocol):

    def onConnect(self, response):
        pass

    def onOpen(self):
        self.sendMessage("[5,\"OnJsonApiEvent\"]".encode("utf-8"))

    def onClose(self, wasClean, code, reason):
        print(Color.RED + "Closed!" + Color.END)
        reactor._stopping = True
        reactor.callFromThread(
            _threadedselect.ThreadedSelectReactor.stop, reactor)

    def onMessage(self, payload, isBinary):
        if not isBinary:
            decoded = payload.decode('utf8')
            if (len(decoded) == 0):
                return
            json_data = json.loads(decoded)
            if json_data[1] != "OnJsonApiEvent":
                return
            if not json_data:
                return
            read_message(json_data)


class WindowHandler():

    def callback(self, hWinEventHook, event, hwnd, idObject, idChild, dwEventThread, dwmsEventTime):
        length = self.user32.GetWindowTextLengthA(hwnd)
        buff = ctypes.create_string_buffer(length + 1)
        self.user32.GetWindowTextA(hwnd, buff, length + 1)
        title = buff.value.decode('ISO-8859-1')
        if title != '':
            self.function_callback(title, hwnd)

    def focus(self, hwnd):
        self.user32.SetForegroundWindow(hwnd)

    def _run(self):
        EVENT_SYSTEM_FOREGROUND = 0x0003
        WINEVENT_OUTOFCONTEXT = 0x0000

        self.user32 = ctypes.windll.user32
        ole32 = ctypes.windll.ole32

        ole32.CoInitialize(0)

        WinEventProcType = ctypes.WINFUNCTYPE(
            None,
            ctypes.wintypes.HANDLE,
            ctypes.wintypes.DWORD,
            ctypes.wintypes.HWND,
            ctypes.wintypes.LONG,
            ctypes.wintypes.LONG,
            ctypes.wintypes.DWORD,
            ctypes.wintypes.DWORD
        )

        WinEventProc = WinEventProcType(self.callback)

        self.user32.SetWinEventHook.restype = ctypes.wintypes.HANDLE
        hook = self.user32.SetWinEventHook(
            EVENT_SYSTEM_FOREGROUND,
            EVENT_SYSTEM_FOREGROUND,
            0,
            WinEventProc,
            0,
            0,
            WINEVENT_OUTOFCONTEXT
        )
        if hook == 0:
            print('SetWinEventHook failed')
            sys.exit(1)

        msg = ctypes.wintypes.MSG()
        while self.user32.GetMessageW(ctypes.byref(msg), 0, 0, 0) != 0:
            self.user32.TranslateMessageW(msg)
            self.user32.DispatchMessageW(msg)

        self.user32.UnhookWinEvent(hook)
        ole32.CoUninitialize()

    def __init__(self, function_callback):
        self.function_callback = function_callback
        t = Thread(target=self._run)
        t.setDaemon(True)
        t.start()


class Color:
    """ Color enumeration class """

    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def params(argv):
    try:
        opts, args = getopt.getopt(
            argv, ":c:b:r:m:", ['champion=', 'ban=', 'reset=', 'mode='])

        for opt, arg in opts:
            if opt in ('-r', '--reset'):
                global arg_reset
                arg_reset = arg
            if opt in ('-c', '--champion'):
                global arg_champ
                arg_champ = arg
            if opt in ('-b', '--ban'):
                global arg_ban
                arg_ban = arg
            if opt in ('-m', '--mode'):
                global arg_mode
                arg_mode = arg
    except getopt.GetoptError:
        pass


def read_message(json_message):
    event_payload = json_message[2]
    uri = event_payload.get('uri').replace('/', '_').replace('-', '_').replace('@', '_at_')\
        .replace('%', '_url_').replace(':', '_')[1:]
    if (DEBUG):
        create_uri_report(uri, json.dumps(event_payload['data']))
    try:
        if ".pvp.net_messages" in uri:
            uri_method = eval("pvp_net_messages")
        else:
            uri_method = eval(uri)
        uri_method(event_payload['data'])
    except Exception as e:
        if not DEBUG:
            return
        if isinstance(e, NameError):
            print(Color.RED + "Event {} not registered".format(uri) + Color.END)
        else:
            #raise e
            print(Color.RED + "################\n{} raised: {}\nCause: {}\n################"
                  .format(uri, e.__class__, e) + Color.END)


def create_uri_report(uri, json):
    global session
    global order
    if not session:
        dateTimeObj = datetime.now()
        timestampStr = dateTimeObj.strftime("%d-%b-%Y-%H-%M-%S")
        session = timestampStr

    try:
        os.mkdir("remote")
    except:
        pass
    try:
        os.mkdir("remote/{}".format(session))
    except:
        pass

    order += 1
    try:
        f = open("remote/{}/{}.{}.json".format(session, order, uri), "w+")
        f.write(str(json))
        f.close()
    except Exception as e:
        print(e)


def parse_message_command(body, sender):
    commands = {
        'go': 'find_match()',
        'pera': 'cancel_queue()',
        'chama': 'cancel_queue_and_invite(sender)'
    }
    try:
        eval(commands[body])
    except:
        pass

# LCU websocket functions


def lol_lobby_v2_received_invitations(json_message):
    pass


def pvp_net_messages(json_message):
    # print(json_message)
    if isinstance(json_message, list):
        return
    if json_message['type'] == 'system':
        pass
    get_friends()
    from_id = json_message['fromSummonerId']
    body = json_message['body']
    now = datetime.now().strftime("%H:%M:%S")
    color = Color.PURPLE if json_message['type'] == 'system' else Color.GREEN
    if from_id == summonerId:
        print(color + "[{}] You: {}".format(now, body) + Color.END)
        send_message('f01e4e36-0b6f-587e-a572-89a0945372c6@br1.pvp.net', '')
    else:
        try:
            print(color +
                  "[{}] {}: {}".format(now, friends[from_id], body) + Color.END)
            parse_message_command(body.lower(), from_id)
        except:
            print(color +
                  "[{}] {}: {}".format(now, from_id, body) + Color.END)


def lol_chat_v1_me(json_message):
    global summonerId
    summonerId = json_message['summonerId']


def lol_matchmaking_v1_search(json_message):
    """ Queue time """
    global reset_time
    if not json_message:
        return
    time = json_message['timeInQueue']
    if (time >= int(reset_time) - 1):
        print(Color.YELLOW + "Queue reset" + Color.END)
        cancel_queue()
        find_match()


def riot_messaging_service_v1_message_teambuilder_v1_tbdGameDtoV1(json_message):
    """ Match found """
    global should_refoucus
    should_refoucus = True
    payload = json.loads(json_message['payload'])
    if payload['phaseName'] == 'AFK_CHECK':
        print(Color.YELLOW + "Match found!" + Color.END)
        accept_match()


def lol_lobby_v2_lobby_matchmaking_search_state(json_message):
    errors = json_message['errors']
    if len(errors) == 0:
        return
    remaining = errors[0]['penaltyTimeRemaining']
    minutes = int(remaining / 60)
    seconds = int(remaining % 60)
    if minutes == 0 and seconds == 1:
        print()
        sleep(2)
        find_match()
    print('\r' + Color.YELLOW + "Dodge -> -{}:{}".format("0" + str(minutes) if minutes < 10 else minutes,
                                                         "0" + str(seconds) if seconds < 10 else seconds) + Color.END, end="")


def lol_loot_v1_ready(json_message):
    print(Color.CYAN + "LCU flashed" + Color.END)
    start_match_on_load()


def lol_champ_select_v1_session(json_message):
    """ Champion select state changed """
    global pick_action
    global has_banned
    global pick
    global ban
    global should_refoucus
    #global has_collected_ids
    #if not has_coolected_ids: collect_and_check_ids(json_message)
    local_player_cell_id = json_message['localPlayerCellId']
    action_list = json_message['actions']
    pick_action = find_local_pick_action(action_list, local_player_cell_id)
    phase = json_message['timer']['phase']
    if phase == 'BAN_PICK':
        if not has_banned:
            should_refoucus = True
            ban_action = find_local_ban_action(
                action_list, local_player_cell_id)
            if ban_action['completed']:
                has_banned = True
            else:
                patch_action(ban, ban_action['id'])
                confirm_action(ban_action['id'])
                print(Color.YELLOW + "Banned {}".format(ban) + Color.END)
        else:
            if pick_action['completed']:
                should_refoucus = False
                print(Color.YELLOW + "Picked {}".format(pick) + Color.END)
                return
            prev = find_previous_turn(action_list, pick_action['id'])
            if is_turn_complete(prev):
                should_refoucus = True
                confirm_action(pick_action['id'])
    elif phase == 'PLANNING':
        if not has_banned:
            patch_action(pick, pick_action['id'])
    else:
        has_banned = False

# LCU HTTP request functions


def get_friends():
    friend_list = json.loads(requests.get("https://{}:{}/lol-chat/v1/friends".format(host, port),
                                          verify=False, headers=headers).content)
    for i in range(0, len(friend_list)):
        f = friend_list[i]
        friends[f['summonerId']] = f['name']


def find_match():
    requests.post("https://{}:{}/lol-lobby/v2/lobby/matchmaking/search".format(host, port),
                  verify=False, headers=headers)


def cancel_queue():
    requests.delete("https://{}:{}/lol-lobby/v2/lobby/matchmaking/search".format(host, port),
                    verify=False, headers=headers)


def create_lobby(queue_id):
    body = {'queueId': queue_id}
    requests.post("https://{}:{}/lol-lobby/v2/lobby".format(host, port),
                  verify=False, headers=headers, data=json.dumps(body))


def choose_positions(primary, secondary):
    global positions
    body = {'firstPreference': positions[primary],
            'secondPreference': positions[secondary]}
    requests.put("https://{}:{}/lol-lobby/v2/lobby/members/localMember/position-preferences".format(host, port),
                 verify=False, headers=headers, data=json.dumps(body))


def accept_match():
    global has_banned
    has_banned = False
    requests.post("https://{}:{}/lol-matchmaking/v1/ready-check/accept".format(host, port),
                  verify=False, headers=headers)


def patch_action(champion_id, action_index):
    body = {'championId': champion_id}
    requests.patch("https://{}:{}/lol-champ-select/v1/session/actions/{}".format(host, port, action_index),
                   verify=False, headers=headers, data=json.dumps(body))


def confirm_action(action_index):
    requests.post("https://{}:{}/lol-champ-select/v1/session/actions/{}/complete".format(host, port, action_index),
                  verify=False, headers=headers)


def get_champions():
    versions = json.loads(requests.get(
        "https://ddragon.leagueoflegends.com/api/versions.json", verify=False).content)
    return json.loads(requests.get("http://ddragon.leagueoflegends.com/cdn/{}/data/en_US/champion.json".format(versions[0]),
                                   verify=False).content)


def invite_player(summoner_id):
    body = [{'toSummonerId': summoner_id}]
    requests.post("https://{}:{}/lol-lobby/v2/lobby/invitations".format(host, port),
                  verify=False, headers=headers, data=json.dumps(body))


def send_message(chat_id, message):
    body = {
        'id': chat_id,
        'inviterId': '',
        'isMuted': False,
        'lastMessage': {
            'body': message,
            'fromId': chat_id,
            'id': '',
            'isHistorical': True,
            'timestamp': '2020-08-18T04:57:02.035Z',
            'type': 'chat'
        },
        'name': '',
        'password': '',
        'type': 'chat',
        'unreadMessageCount': 0
    }
    requests.put(
        "https://{}:{}/lol-chat/v1/conversations/{}".format(
            host, port, chat_id),
        verify=False, headers=headers, data=json.dumps(body))


def cancel_queue_and_invite(id):
    cancel_queue()
    invite_player(id)


def find_local_pick_action(action_list, local_cell_id):
    for turn in action_list:
        for action in turn:
            if action['actorCellId'] == local_cell_id and action['type'] == 'pick':
                return action
    return None


def find_local_ban_action(action_list, local_cell_id):
    for turn in action_list:
        for action in turn:
            if action['actorCellId'] == local_cell_id and action['type'] == 'ban':
                return action
    return None


def get_champion_dict():
    champions = get_champions()['data']
    champ_dict = {}
    for champ in champions:
        champ_dict[str(champions[champ]['id']).lower()
                   ] = champions[champ]['key']
    return champ_dict


def find_previous_turn(action_list, action_id):
    prev_turn = None
    for turn in action_list:
        for action in turn:
            if action['id'] == action_id:
                return prev_turn
        prev_turn = turn
    return prev_turn


def is_turn_complete(turn):
    return all(action['completed'] for action in turn)


def find_procs_by_name(name):
    for proc in psutil.process_iter():
        if proc.name() == name:
            return proc.pid


def start_match_on_load():
    global pick
    global ban
    global reset_time
    pick = champs['katarina'] if not arg_champ else champs[arg_champ]
    ban = champs['akali'] if not arg_ban else champs[arg_ban]
    reset_time = 1 * 60 if not arg_reset else arg_reset
    print(Color.BLUE + "Pick: {} Ban: {}".format(arg_champ, arg_ban))
    print("Resetting in {}".format(reset_time) + Color.END)
    create_lobby(queues['normal'] if not arg_mode else queues[arg_mode])
    choose_positions('mid', 'adc')
    find_match()


def window_changed(window_title, hwnd):
    global last_hwnd
    global should_refoucus
    if (should_refoucus):
        win_mgr.focus(last_hwnd)
        should_refoucus = False
    last_hwnd = hwnd
    #print(Color.DARKCYAN + "Window manager -> Changed to {}".format(window_title) + Color.END)


def collect_and_check_ids(json):
    data = {}
    past_mates = open('mate_history.json', 'w+')
    if past_mates:
        data = json.load(past_mates)
    myteam = json['myTeam']
    for mate in myteam:
        if mate['cellId'] == json['localPlayerCellId']:
            pass
        if data[mate['summonerId']]:
            print('')


if __name__ == '__main__':
    try:
        init()
        host = "127.0.0.1"
        pid = str(find_procs_by_name("LeagueClientUx.exe"))
        should_start = False

        if pid == str(None):
            print(Color.RED + "League Client not open! Opening..." + Color.END)
            subprocess.Popen("\"C:\Riot Games\Riot Client\RiotClientServices.exe\"\
                --launch-product=league_of_legends --launch-patchline=live")
            while find_procs_by_name("LeagueClientUx.exe") == None:
                print(
                    Color.RED + "Waiting for LeagueClientUx.exe to come alive" + Color.END)
                sleep(3)
            pid = str(find_procs_by_name("LeagueClientUx.exe"))
            print(Color.CYAN + "Found PID {}".format(pid) + Color.END)
        else:
            should_start = True
            print(Color.CYAN + "Found PID {}".format(pid) + Color.END)

        print(Color.CYAN + "Parsing proccess command line..." + Color.END)
        wmic_output = str(subprocess.check_output(
            "wmic process where \"processId={}\" get commandLine".format(pid)))
        split_output = wmic_output.split(" ")
        r = re.compile("^\"--app-port*")
        port = int(list(filter(r.match, split_output))
                   [0].split('=')[1].replace("\"", ""))
        print(Color.CYAN + "Port: {}".format(port) + Color.END)

        r = re.compile("^\"--remoting-auth-token*")
        token = list(filter(r.match, split_output))[
            0].split('=')[1].replace("\"", "")
        print(Color.CYAN + "Token: {}".format(token) + Color.END)
        encodedBytes = base64.b64encode(
            "riot:{}".format(token).encode("utf-8"))
        auth = str(encodedBytes, "utf-8")
        print(Color.CYAN + "Auth: {}".format(auth) + Color.END)

        headers = {"Authorization": "Basic {}".format(auth)}

        #print(lol_watcher.summoner.by_name(my_region, 'sadmlra'))

        factory = WebSocketClientFactory(
            "wss://riot:{}@{}:{}".format(token, host, port), headers=headers)
        factory.protocol = LeagueClientProtocol

        connectWS(factory)

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        champs = get_champion_dict()
        params(sys.argv[1:])

        if should_start:
            start_match_on_load()
        else:
            print(Color.YELLOW +
                  "Waiting for LCU flash before starting match..." + Color.END)

        win_mgr = WindowHandler(window_changed)
        reactor.run()
    except KeyboardInterrupt:
        print(Color.RED + "Interrupt" + Color.END)
