#!/usr/bin/python

import configparser
import os
import subprocess
import sys
from random import choice

import clipboard
from asciimatics.effects import Print
from asciimatics.exceptions import ResizeScreenError, NextScene
from asciimatics.renderers import Plasma, FigletText
from asciimatics.scene import Scene
from asciimatics.screen import Screen
from asciimatics.widgets import Frame, Layout, Button, ListBox, Widget, Divider, Label, Text, RadioButtons


class ConfigManager:
    def __init__(self):
        self._path = os.getcwd()

    def read_config(self, section, prop):
        _pwd = os.getcwd()
        os.chdir(self._path)
        os.chdir("ginter")
        cfgfile = open("ginter.conf", "r")
        config = configparser.ConfigParser()
        config.read("ginter.conf")
        cfgfile.close()
        os.chdir(_pwd)
        if not config.has_section(section):
            return -1
        return config.get(section, prop)

    def write_config(self, section, prop, value):
        _pwd = os.getcwd()
        os.chdir(self._path)
        os.chdir("ginter")
        cfgfile = open("ginter.conf", 'a')
        config = configparser.ConfigParser()
        config.read("ginter.conf")
        if not (config.has_section(section)):
            config.add_section(section)
        config.set(section, prop, value)
        cfgfile.seek(0)
        cfgfile.truncate()
        config.write(cfgfile)
        cfgfile.close()
        os.chdir(_pwd)


class DefaultOptions(object):
    menu_main = "__main__"
    menu_setup = "__setup__"
    menu_search = "__search__"

    def __init__(self):
        self.main_menu_options = [
            ['Reset "res" folder and platform type', self.menu_setup],
            ['Add string to project', self.menu_search],
            ['Add custom string', 'add_custom']
        ]

        self.search_options = [
            ['No items', 1]
        ]


class LoadingScreen(Scene):
    _comments = [
        "Loading...",
        "__No_XGH__",
        "Strings..",
        "Feeding DOGES",
        "WAIT!!",
        "WOW"
    ]

    def __init__(self, screen):
        self._screen = screen

        effects = [
            Print(screen,
                  Plasma(screen.height, screen.width, 255),
                  0,
                  speed=1,
                  transparent=False),
        ]
        super(LoadingScreen, self).__init__(effects, 50, clear=False)

    def _add_cheesy_comment(self):
        msg = FigletText(choice(self._comments))
        self._effects.append(
            Print(self._screen,
                  msg,
                  (self._screen.height // 2) - 4,
                  x=(self._screen.width - msg.max_width) // 2 + 1,
                  colour=Screen.COLOUR_WHITE,
                  stop_frame=0,
                  speed=1))

    def reset(self, old_scene=None, screen=None):
        super(LoadingScreen, self).exit()
        self._effects = [self._effects[0]]
        self._add_cheesy_comment()


class MainMenu(Frame):
    def __init__(self, screen):
        super(MainMenu, self).__init__(screen, screen.height * 4 // 4, screen.width * 4 // 4, hover_focus=False,
                                       title="Main menu", reduce_cpu=False)

        self._list_view = ListBox(
            Widget.FILL_FRAME,
            options.main_menu_options,
            name="main_menu")

        header = Layout([screen.width, 1, 1, 1])
        self.add_layout(header)
        self._header_label = Label('')
        header.add_widget(self._header_label)
        header.add_widget(Divider())
        header.focus()

        layout = Layout([100], fill_frame=True)
        self.add_layout(layout)
        layout.add_widget(self._list_view)
        layout.add_widget(Divider())
        layout2 = Layout([1, 1])
        self.add_layout(layout2)
        layout2.add_widget(Button("Quit", self._quit), 0)
        layout2.add_widget(Button("Ok", self._ok), 1)
        self.fix()

    @staticmethod
    def _quit():
        sys.exit(0)

    def _ok(self):
        raise NextScene(self._list_view.value)

    def reset(self):
        self._header_label._text = 'ResourceDir: %s' % config_manager.read_config('DestRes', "dir")
        super(MainMenu, self).reset()


class SetupGinter(Frame):
    def __init__(self, screen):
        super(SetupGinter, self).__init__(screen, screen.height * 4 // 4, screen.width * 4 // 4, hover_focus=False,
                                          title="Setup", reduce_cpu=False)
        self._setup_view(screen)

    def _setup_view(self, screen):
        self._dest_res = None
        header = Layout([screen.width, 1, 1, 1])
        self.add_layout(header)
        header.add_widget(Label('Current res path: '))
        header.add_widget(Divider())

        layout = Layout([8, 1], fill_frame=True)
        self.add_layout(layout)

        self._res_path_text = Text("New res path:", "res_path")
        layout.add_widget(self._res_path_text, 0)
        layout.add_widget(Button("Paste", self._paste), 1)
        layout.add_widget(Divider())

        radio_options = [
            ['Tablet', 1],
            ['Smart', 2]
        ]

        self._radio_buttons = RadioButtons(radio_options, 'Platform', 'platform')

        layout.add_widget(self._radio_buttons)

        layout2 = Layout([1, 1])
        self.add_layout(layout2)
        layout2.add_widget(Button("Cancel", self._cancel), 0)
        layout2.add_widget(Button("Ok", self._ok), 1)
        self.fix()

    def _paste(self):
        self._res_path_text.value = clipboard.paste()

    def _ok(self):
        config_manager.write_config('DestRes', "dir", self._res_path_text.value)
        config_manager.write_config('DestRes', "plat", str(self._radio_buttons.value))
        raise NextScene(options.menu_main)

    @staticmethod
    def _cancel():
        raise NextScene(options.menu_main)

    def reset(self):
        super(SetupGinter, self).reset()
        self._radio_buttons.value = int(config_manager.read_config('DestRes', 'plat'))
        self._res_path_text.value = config_manager.read_config('DestRes', "dir")


class SearchString(Frame):
    def __init__(self, screen):
        super(SearchString, self).__init__(screen, screen.height * 4 // 4, screen.width * 4 // 4, hover_focus=True,
                                           title="Search string", reduce_cpu=False)
        self._configure_files()

        header = Layout([screen.width, 1, 1, 1])
        self.add_layout(header)
        self._string_text = Text('String to search: ', on_change=self._on_change)
        header.add_widget(self._string_text)
        header.add_widget(Divider())

        self._list_view = ListBox(
            Widget.FILL_FRAME,
            options.search_options,
            name="search_result")

        layout = Layout([100], fill_frame=True)
        self.add_layout(layout)
        layout.add_widget(self._list_view)

        layout2 = Layout([1, 1])
        self.add_layout(layout2)
        layout2.add_widget(Button("Back", self._back), 0)
        layout2.add_widget(Button("Next", self._next), 1)

        self.fix()

    @staticmethod
    def _back():
        raise NextScene(options.menu_main)

    def _next(self):
        pass

    def _on_change(self):
        self._list_view.reset()
        options.search_options.clear()
        for i, line in enumerate(self._lines):
            if self._string_text.value.lower() in line.lower():
                options.search_options.append([line.replace('\n', ''), i])
                self._list_view.options = options.search_options

    def _configure_files(self):
        if config_manager.read_config('DestRes', 'plat') == '1':
            os.chdir("ginter/translation/iPad")
            self._strfile = 'iOS_project_English_US.strings'
        else:
            os.chdir("ginter/translation/iPhone")
            self._strfile = 'iOS_smart_Portuguese_BR.strings'
        f = open(self._strfile, "r", encoding="utf8")
        self._lines = f.readlines()

        for i, x in enumerate(self._lines):
            if '=' not in x:
                self._lines.remove(x)
            else:
                self._lines[i] = x.split('=')[1].replace('"', '').replace(';', '').strip()

        f.close()
        os.chdir("../../..")

    def reset(self):
        super(SearchString, self).reset()


def demo(screen, scene):
    _setup_scene = Scene([SetupGinter(screen)], -1, name=options.menu_setup)

    scenes = [
        LoadingScreen(screen),
        Scene([MainMenu(screen)], -1, name=options.menu_main),
        Scene([SearchString(screen)], -1, name=options.menu_search)
    ]

    if not os.path.exists("ginter\ginter.conf"):
        scenes.insert(0, _setup_scene)
    else:
        scenes.append(_setup_scene)

    screen.play(scenes, stop_on_resize=True, start_scene=scene)


def create_structure():
    if not os.path.isdir("ginter"):
        os.makedirs("ginter")
    os.chdir("ginter")
    if not os.path.isdir("translation"):
        subprocess.call(["git", "clone", "https://github.com/its4company/translation.git"], shell=False)
    os.chdir('..')


def update_translations():
    os.chdir("translation")
    subprocess.call(["git", "fetch"])
    out = subprocess.check_output(["git", "diff", "origin/master"])
    if out:
        subprocess.call(["git", "reset", "--hard", "origin/master"])
    os.chdir("..")


if __name__ == "__main__":

    create_structure()
    # update_translations()

    options = DefaultOptions()
    config_manager = ConfigManager()
    last_scene = None

    while True:
        try:
            Screen.wrapper(demo, catch_interrupt=False, arguments=[last_scene])
            sys.exit(0)
        except ResizeScreenError as e:
            last_scene = e.scene
