#!/usr/bin/python
import configparser
import json
import os
import subprocess
import sys

import requests
from colorama import init

yandex_api_key = 'trnsl.1.1.20170207T193920Z.4f3d25df809096ae.2fe7efd3de83dfe01a4d099ba78e2973641f3833'


class Color:
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    END = '\033[0m'


def search_menu():
    string = ""
    while not string:
        print(Color.YELLOW + "\nEnter the string to search: " + Color.END, end="")
        string = input()

    if read_config('DestRes', 'plat') == '1':
        os.chdir("translation/iPad")
        strfile = 'iOS_project_English_US.strings'
    else:
        os.chdir("translation/iPhone")
        strfile = 'iOS_smart_Portuguese_BR.strings'

    f = open(strfile, "r", encoding="utf8")
    lines = f.readlines()
    f.close()
    results = []
    for i, line in enumerate(lines):
        if string.lower() in line.lower():
            results.append(line + "~" + str(i))

    if len(results) == 0:
        print("String " + Color.RED + Color.BOLD + string + Color.END + " not found")
        os.chdir("..")
        os.chdir("..")
        input()
        return
    for i, l in enumerate(results):
        print(Color.RED + Color.BOLD + "[" + str(i + 1) + "] " + Color.GREEN, end='')
        uprint(l.split("=")[1].split(";")[0])

    print(Color.RED + Color.BOLD + "[0]" + Color.YELLOW + "  Back")

    choice = ""
    while not choice or not (-1 < int(choice) <= len(results)):
        print(Color.END + "Enter your choice: ", end="")
        choice = input()

    if int(choice) == 0:
        os.chdir("..")
        os.chdir("..")
        return

    new_key = ""
    while not new_key:
        print("Enter the new key: its4_", end="")
        new_key = input()

    write_new_strings("its4_" + new_key, index_new_strings(results[int(choice) - 1].split("=")[0]))


def index_new_strings(base_key):
    os.chdir(path + "/ginter/translation")
    indexes = []
    if plat_num == 1:
        indexes.append(find_section_line("iPad/iOS_project_English_US.strings", base_key))
        indexes.append(find_section_line("iPad/iOS_project_Dutch.strings", base_key))
        indexes.append(find_section_line("iPad/iOS_project_Italian.strings", base_key))
        indexes.append(find_section_line("iPad/iOS_project_Portuguese.strings", base_key))
    else:
        indexes.append(find_section_line("iPhone/iOS_smart_English_US.strings", base_key))
        indexes.append(find_section_line("iPhone/iOS_smart_Dutch.strings", base_key))
        indexes.append(find_section_line("iPhone/iOS_smart_Italian.strings", base_key))
        indexes.append(find_section_line("iPhone/iOS_smart_Portuguese_BR.strings", base_key))
    return indexes


def find_section_line(string_file, section):
    sf = open(string_file, "r+", encoding="utf8")
    lines = sf.readlines()
    for i, l in enumerate(lines):
        if section in l:
            sf.close()
            return i
    lines.insert(len(lines) - 1, "\n" + section)
    sf.truncate(0)
    sf.seek(0)
    sf.writelines(lines)
    sf.close()
    return len(lines) - 1


def write_new_strings(new_key, new_strings_lines):
    if plat_num == 1:
        str_dirs = {
            "/values-en/": "iPad/iOS_project_English_US.strings:" + str(new_strings_lines[0]),
            "/values-nl/": "iPad/iOS_project_Dutch.strings:" + str(new_strings_lines[1]),
            "/values-it/": "iPad/iOS_project_Italian.strings:" + str(new_strings_lines[2]),
            "/values/": "iPad/iOS_project_Portuguese.strings:" + str(new_strings_lines[3])
        }
    else:
        str_dirs = {
            "/values/": "iPhone/iOS_smart_English_US.strings:" + str(new_strings_lines[0]),
            "/values-nl/": "iPhone/iOS_smart_Dutch.strings:" + str(new_strings_lines[1]),
            "/values-it/": "iPhone/iOS_smart_Italian.strings:" + str(new_strings_lines[2]),
            "/values-pt/": "iPhone/iOS_smart_Portuguese_BR.strings:" + str(new_strings_lines[3])
        }

    print("\n")
    for k, v in str_dirs.items():
        new_str = get_str_in_line(path + "/ginter/translation/" + v.split(":")[0], v.split(":")[1])
        if contains_in_file(dest_res + k + "strings.xml", '>{0}<'.format(new_str.replace(
                '\n', '').replace('"', '').strip())):
            print(Color.RED + Color.BOLD, end='')
            print("String %s already exists in %s" % ((new_str.replace('\n', '').replace('"', '').strip()), k), end='')
            print("... Adding anyway" + Color.END)
        if contains_in_file(dest_res + k + "strings.xml", '"{0}"'.format(new_key)):
            print(Color.RED + Color.BOLD + "Key already exists" + Color.END)
            break
        create_string_in_file(dest_res + k + "strings.xml", new_str, new_key)
    print("\n")
    print_info("Process completed")
    input()
    return


def contains_in_file(file, text):
    if text in open(file).read():
        return True


def get_str_in_line(file, line):
    sf = open(file, "r", encoding="utf8")
    lines = sf.readlines()
    sf.close()
    for i, l in enumerate(lines):
        if i == int(line):
            return l.split("=")[1].replace(';', '')


def create_string_in_file(string_file, content, newkey):
    strfile = open(string_file, "r+", encoding="utf-8")
    lines = strfile.readlines()
    lines.insert(len(lines) - 1,
                 "    <string name=\"" + newkey + "\">" +
                 str(content).replace('"', '').replace("\'", "\\'").strip() + "</string>\n")
    strfile.truncate(0)
    strfile.seek(0)
    strfile.writelines(lines)
    strfile.close()
    print(Color.GREEN + Color.BOLD + "<string name=\"" + newkey + "\">" + Color.END, end='')
    uprint(str(content).replace('"', '').strip(), end='')
    print(Color.GREEN + Color.BOLD + "</string>", end=Color.END + '\n')


def uprint(*objects, sep=' ', end='\n', file=sys.stdout):
    enc = file.encoding
    if enc == 'UTF-8':
        print(*objects, sep=sep, end=end, file=file)
    else:
        f = lambda obj: str(obj).encode(enc, errors='backslashreplace').decode(enc)
        print(*map(f, objects), sep=sep, end=end, file=file)


def check_dest_res():
    global dest_res, plat, plat_num
    dest_res = ""

    if not (os.path.exists("ginter.conf")):
        while not dest_res or not os.path.isdir(dest_res):
            print(Color.RED + "Please, enter the 'res' directory of your project: " + Color.END, end="")
            dest_res = input()
        write_config('DestRes', "dir", dest_res)
        set_project()

    dest_res = read_config('DestRes', "dir")
    plat_num = int(read_config('DestRes', "plat"))
    plat = "Tablet" if plat_num == 1 else "SmartPhone"


def main_menu():
    clear()
    os.chdir(path + "/ginter")
    print_info("Current 'res' directory: " + Color.RED + dest_res)
    print_info("Platform: " + plat)
    print(Color.BOLD + Color.CYAN + "\n\n1 - Set \"res\" destination directory and platform")
    print("2 - Add string to project")
    print("3 - Add custom string")
    print("4 - Exit\n" + Color.END)
    choice = ""
    while not choice or not (choice == '1' or choice == '2' or choice == '3' or choice == '4'):
        print("> ", end="")
        choice = input()
    if choice == '1':
        os.remove("ginter.conf")
        check_dest_res()
        return
    elif choice == '2':
        search_menu()
    elif choice == '3':
        add_custom_string()
    elif choice == '4':
        sys.exit(0)


def add_custom_string():
    print("Enter the custom string in " + Color.BOLD + Color.CYAN + "English: " + Color.END, end='')
    text = ""
    while not text:
        text = input()

    if plat_num == 1:
        str_dirs = ["/values-en/", "/values-nl/", "/values-it/", "/values/"]
    else:
        str_dirs = ["/values/", "/values-nl/", "/values-it/", "/values-pt/"]

    translations = translate_text(text)
    print(Color.BOLD + Color.RED, end='')
    print(translations, end=Color.END + '\n')

    new_key = ""
    while not new_key:
        print("Enter the new key: its4_auto_", end="")
        new_key = input()

    print('\n')
    for i, a_dir in enumerate(str_dirs):
        create_string_in_file(dest_res + a_dir + "strings.xml", translations[i], 'its4_auto_' + new_key)

    print('\n')
    print_info("Process completed")
    input()


def translate_text(text):
    langs = ['nl', 'it', 'pt']
    results = [text] if plat_num != 1 else []
    for lang in langs:
        rq_url = 'https://translate.yandex.net/api/v1.5/tr.json/translate?key={0}&text={1}&lang={2}'
        data = json.loads(requests.get(rq_url.format(yandex_api_key, text, 'en-' + lang)).text)
        results.append(data['text'][0])
    return results


def set_project():
    choice = ""
    while not choice or not (choice == '1' or choice == '2'):
        print(Color.RED + Color.BOLD + "[1] " + Color.GREEN + "Tablet")
        print(Color.RED + Color.BOLD + "[2] " + Color.GREEN + "Smart")
        print("What platform " + Color.GREEN + dest_res + Color.END + " belongs to? ", end="")
        choice = input()
    write_config('DestRes', "plat", choice)


def read_config(section, prop):
    pwd = os.getcwd()
    os.chdir(path)
    os.chdir("ginter")
    cfgfile = open("ginter.conf", "r")
    config = configparser.ConfigParser()
    config.read("ginter.conf")
    cfgfile.close()
    os.chdir(pwd)
    if not config.has_section(section):
        return -1
    return config.get(section, prop)


def write_config(section, prop, value):
    pwd = os.getcwd()
    os.chdir(path)
    os.chdir("ginter")
    cfgfile = open("ginter.conf", 'a')
    config = configparser.ConfigParser()
    config.read("ginter.conf")
    if not (config.has_section(section)):
        config.add_section(section)
    config.set(section, prop, value)
    cfgfile.seek(0)
    cfgfile.truncate()
    config.write(cfgfile)
    cfgfile.close()
    os.chdir(pwd)


def print_info(message):
    print(Color.BOLD + Color.GREEN + "[+] {0}".format(message) + Color.END)


def create_structure():
    if not os.path.isdir("ginter"):
        os.makedirs("ginter")
    os.chdir("ginter")
    if not os.path.isdir("translation"):
        print_info("Cloning project...")
        subprocess.call(["git", "clone", "https://github.com/its4company/translation.git"], shell=False)


def clear():
    if sys.platform == 'win32':
        os.system('cls')
    else:
        os.system('clear')


def update_translations():
    os.chdir("translation")
    print_info("Updating repository")
    subprocess.call(["git", "fetch"])
    out = subprocess.check_output(["git", "diff", "origin/master"])
    if out:
        subprocess.call(["git", "reset", "--hard", "origin/master"])
    print_info("Repository is up-to-date")
    os.chdir("..")


if __name__ == "__main__":
    global path
    path = os.getcwd()
    try:
        init()
        create_structure()
        update_translations()
        check_dest_res()
        while 1:
            try:
                main_menu()
            except ValueError:
                pass
    except KeyboardInterrupt:
        print("\b\b" + Color.BOLD + Color.RED + "Program interrupted!" + Color.END)
        try:
            sys.exit(0)
        except SystemExit:
            # noinspection PyProtectedMember
            os._exit(0)
