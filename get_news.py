from bs4 import BeautifulSoup
from os import path
from os import system
import os.path
import requests
import os
import pyttsx3
import webbrowser
import signal
import time

def get_plain(url):
    code = requests.get(url, headers={
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.5',
        'Cache-Control': 'max-age=0',
        'Connection': 'keep-alive',
        'Cookie': 'PreferencesMsn=eyJIb21lUGFnZSI6eyJTdHJpcGVzIjpbXSwiTWVTdHJpcGVNb2R1bGVzIjpbXSwiTWFya2V0Q29uZmlndXJhdGlvbiI6eyJNYXJrZXQiOiJwdC1iciIsIlN1cHByZXNzUHJvbXB0IjpmYWxzZSwiUHJlZmVycmVkTGFuZ3VhZ2VDb2RlIjoiZW4tdXMiLCJDb3VudHJ5Q29kZSI6IkJSIn19LCJFeHBpcnlUaW1lIjo2Mzc0NzQ5MzE5NDYzOTY2MDAsIlZlcnNpb24iOjF90; marketPref=pt-br; _EDGE_V=1; MUID=1EFF65935180656A181C6BCA503F64AC; timeZoneOffsetInMins=180; _SS=SID=00; anoncknm=; dpio=1.8',
        'DNT': '1',
        'Host': 'www.msn.com',
        'Referer': 'https://www.msn.com/pt-br/noticias/brasil',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'
    })
    return code.text

def save_news(news_key):
    news_file = open("news_file.txt", 'a')
    news_file.write(f'{news_key}\n')

def get_read_news():
    if (path.exists("news_file.txt")):
        news_file = open("news_file.txt", 'r')
    else:
        open("news_file.txt", 'w')
        news_file = open("news_file.txt", 'r')
    return news_file.readlines()

def read_news(url):
    plain = get_plain(url)
    s = BeautifulSoup(plain, "html.parser")
    main_content = s.find_all('li', {'class': 'rc rcp'})
    news_list = []
    for news in main_content:
        news_title = news.find('a').get('aria-label')
        news_source = news.find('a').find('span', {'class': 'sourcetitle'})
        title = news_title.split('-artigo')[0]
        source = news_source.get('title')
        link = 'https://www.msn.com{}'.format(news.find('a').get('href'))
        news_list.append('{}*{}*{}'.format(source, title, link))

    read_news = get_read_news()

    for news in news_list:
        if '{}\n'.format(news) in read_news:
            continue
        save_news(news)
        source = news.split('*')[0]
        title = news.split('*')[1]
        link = news.split('*')[2]
        print(source)
        print(link)
        print('{}\n--------------'.format(title))
        engine.say("{}. Por {}.".format(title, source))
        engine.runAndWait()


if __name__ == "__main__":
    engine = pyttsx3.init()
    engine.setProperty('voice', 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_PT-BR_MARIA_11.0')

while(True):
    system('clear')
    read_news("https://www.msn.com/pt-br/noticias/mundo")
    read_news("https://www.msn.com/pt-br/noticias/brasil")
    read_news("https://www.msn.com/pt-br/noticias/politica")
    time.sleep(300)