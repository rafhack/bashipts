#!/usr/bin/python3
""" Stitch Automator """

import datetime
import getopt
import json
import os
import subprocess
import sys
import winreg
import uuid

import requests
import urllib3

from xml.etree.ElementTree import Element, SubElement
from xml.etree import ElementTree as etree
from xml.dom import minidom
from getpass import getpass

SUB_KEY = "Software\HNDSofts\Stitch"


def init():
    parse_params()


def parse_params():
    longopts = ["query=", "prepare", "execute=",
                "deactivate=" "activate=", "configure", "delete="]
    shoropts = "q:pe:d:a:cD:"
    try:
        opts, _ = getopt.getopt(sys.argv[1:], shoropts, longopts)
        for opt, arg in opts:
            if opt in ('-q', '--query'):
                query_schedule(arg)
            if opt in ('-p', '--prepare'):
                prepare_schedule()
            if opt in ('-a', '--activate'):
                activate_stitch(arg)
            if opt in ('-e', '--execute'):
                execute_stitch(arg)
            if opt in ('-c', '--configure'):
                configure_auth()
            if opt in ('-D', '--delete'):
                delete_schedule(arg)
            if opt in ('-d', '--deactivate'):
                deactivate_schedule(arg)
        if not opts:
            usage()
    except getopt.GetoptError:
        usage()


def delete_schedule(sch_id):
    confirm = input("Delete {} ? [Y/N] ".format(sch_id))
    if confirm.lower() != 'y':
        return

    delete_schtasks(sch_id, False)
    app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)
    winreg.DeleteKey(app_key, sch_id)
    app_key.Close()


def deactivate_schedule(sch_id):
    confirm = input("Deactivate {} ? [Y/N] ".format(sch_id))
    if confirm.lower() == 'y':
        delete_schtasks(sch_id, True)


def delete_schtasks(sch_id, log):
    try:
        app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)
        sch_key = winreg.OpenKey(app_key, sch_id, 0, winreg.KEY_READ)
        start_times = winreg.QueryValueEx(sch_key, "start_times")[0]
        for time in start_times:
            sufix = time.replace(':', '')
            try:
                subprocess.check_output(
                    "schtasks /DELETE /TN {}-{} /F".format(sch_id, sufix), stderr=subprocess.DEVNULL)
            except subprocess.CalledProcessError:
                if log:
                    print("{} {} is not activated".format(sch_id, time))
        sch_key.Close()
        app_key.Close()
    except OSError:
        print("{} does not exist".format(sch_id))


def configure_auth():
    try:
        app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)
        winreg.SetValueEx(app_key, "employer", 0,
                          winreg.REG_SZ, input("Employer: "))
        winreg.SetValueEx(app_key, "pin", 0, winreg.REG_SZ, input("PIN: "))
        winreg.SetValueEx(app_key, "JSESSIONID", 0,
                          winreg.REG_SZ, input("JSESSIONID: "))
        winreg.SetValueEx(app_key, "USERNAME", 0,
                          winreg.REG_SZ, input("User ID: "))
        winreg.SetValueEx(app_key, "AUTH", 0,
                          winreg.REG_SZ, input("Authorization: "))
        app_key.Close()
    except OSError as e:
        print(e)


def execute_stitch(sch_id):
    try:
        app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)
        sch_key = winreg.OpenKey(app_key, sch_id, 0, winreg.KEY_READ)

        employer = winreg.QueryValueEx(app_key, "employer")[0]
        pin = winreg.QueryValueEx(app_key, "pin")[0]
        session_id = winreg.QueryValueEx(app_key, "JSESSIONID")[0]
        user_id = winreg.QueryValueEx(app_key, "USERNAME")[0]
        auth = winreg.QueryValueEx(app_key, "AUTH")[0]
        skip_dates = winreg.QueryValueEx(sch_key, "skip_dates")[0]

        date = datetime.datetime.today().strftime('%d/%m/%Y')
        if (date in skip_dates):
            print("Skip date, exiting!")
            return

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        baseURL = "https://app.tangerino.com.br/Tangerino/ws"
        headers = {
            "EMPREGADOR": employer,
            "pin": pin,
            "Cookie": "JSESSIONID={}".format(session_id),
            "funcionarioid": user_id,
            "USERNAME": user_id,
            "authorization": auth,
            "Content-Type": "application/json"
        }

        print(requests.get("{}/fingerprintWS/funcionario/empregador/{}/pin/{}"
                           .format(baseURL, employer, pin), verify=False, headers=headers).status_code)

        body = {'deviceId': None}
        print(requests.post("{}/autorizaDipositivoWS/verifica/web/empregador/{}/pin/{}"
                            .format(baseURL, employer, pin), verify=False, headers=headers, data=json.dumps(body)).status_code)

        date_time = datetime.datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        body = {
            "horaInicio": date_time,
            "deviceId": None,
            "online": "true",
            "codigoEmpregador": employer,
            "pin": pin,
            "horaFim": "",
            "tipo": "WEB",
            "foto": "",
            "intervalo": "",
            "validFingerprint": False,
            "versao": "registra-ponto-fingerprint",
            "plataforma": "WEB",
            "funcionarioid": user_id,
            "idAtividade": 6,
            "latitude": None,
            "longitude": None
        }
        print(requests.post("{}/pontoWS/ponto/sincronizacaoPontos/1.2"
                            .format(baseURL), verify=False, headers=headers, data=json.dumps(body)).status_code)
    except:
        print(sys.exc_info()[0])
        #print(e)
        #input()


def activate_stitch(sch_id):
    try:
        app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)
        sch_key = winreg.OpenKey(app_key, sch_id, 0, winreg.KEY_READ)

        start_times = winreg.QueryValueEx(sch_key, "start_times")[0]
        days_of_week = winreg.QueryValueEx(sch_key, "days_of_week")[0]
        skip_dates = winreg.QueryValueEx(sch_key, "skip_dates")[0]

        confirm = input("Start Times: {}\nDays of week: {}\nSkip Dates: {}\n\nActivate schedule {} ? [Y/N] "
                        .format(start_times, days_of_week, skip_dates, sch_id))

        if confirm.lower() != 'y':
            app_key.Close()
            sch_key.Close()
            return

        user_name = os.environ['USERNAME']
        password = getpass(f"Password for user {user_name}: ")
        for time in start_times:
            xml_file = createTaskXML(time, days_of_week, sch_id)
            sufix = time.replace(':', '')
            command = f"schtasks /CREATE /RU {user_name} /RP {password} /TN {sch_id}-{sufix} /XML {xml_file} /F"
            try:
                subprocess.check_output(command, stderr=subprocess.DEVNULL)
            except subprocess.CalledProcessError:
                print("Failed to create schedule (Wrong password?)")
                return
            os.remove(xml_file)

    except OSError as e:
        print("Could not find schedule with the specified id")
        print(e)


def prepare_schedule():
    try:
        app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)

        employer = winreg.QueryValueEx(app_key, "employer")[0]
        pin = winreg.QueryValueEx(app_key, "pin")[0]
        session_id = winreg.QueryValueEx(app_key, "JSESSIONID")[0]
        user_id = winreg.QueryValueEx(app_key, "USERNAME")[0]
    except OSError as e:
        print("Missing configurations. Use -c option")
        print(e)

    print("Configuring for [{}, {}, {}, {}]".format(
        employer, pin, session_id, user_id))

    start_times = ["09:00", "12:00", "13:00", "18:00"]
    input_times = input(
        "Start Times (Separated by colons): {} ".format(start_times))
    start_times = input_times.split(',') if input_times else start_times

    days_of_week = ["MON", "TUE", "WED", "THU", "FRI"]
    input_days_of_week = input(
        "Days of week (Separated by colons): {} ".format(days_of_week))
    days_of_week = input_days_of_week.split(
        ',') if input_days_of_week else days_of_week

    skip_dates = []
    input_skip_dates = input(
        "Skip dates (dd/MM/yyyy): (Separated by colons): {} ".format(skip_dates))
    skip_dates = input_skip_dates.split(
        ',') if input_skip_dates else skip_dates

    confirm = input(
        "Prepare schedule ({}, {}, {}) [Y/N]: ".format(days_of_week, start_times, skip_dates))

    if confirm.lower() != 'y':
        try:
            app_key.Close()
        except OSError as e:
            print(e)
        return

    try:
        sch_id = str(uuid.uuid4())
        sch_key = winreg.CreateKey(app_key, sch_id)

        winreg.SetValueEx(sch_key, "start_times", 0,
                          winreg.REG_MULTI_SZ, start_times)
        winreg.SetValueEx(sch_key, "days_of_week", 0,
                          winreg.REG_MULTI_SZ, days_of_week)
        winreg.SetValueEx(sch_key, "skip_dates", 0,
                          winreg.REG_MULTI_SZ, skip_dates)

        print("Prepared schedule: {}".format(sch_id))

        sch_key.Close()
        app_key.Close()
    except OSError as e:
        print(e)


def query_schedule(sch_id):
    app_key = winreg.CreateKey(winreg.HKEY_CURRENT_USER, SUB_KEY)
    keys = []
    index = 0
    try:
        while index >= 0:
            id = winreg.EnumKey(app_key, index)
            if ((sch_id != '.' and id == sch_id) or sch_id == '.'):
                keys.append(create_query_dict(id, app_key))
            index += 1
    except OSError:
        index = -1
    print(json.dumps(keys, indent=4, sort_keys=True))


def create_query_dict(sch_id, app_key):
    sch_key = winreg.OpenKey(app_key, sch_id, 0, winreg.KEY_READ)
    return {
        "id": sch_id,
        "start_times": winreg.QueryValueEx(sch_key, "start_times")[0],
        "days_of_week": winreg.QueryValueEx(sch_key, "days_of_week")[0],
        "skip_dates": winreg.QueryValueEx(sch_key, "skip_dates")[0]
    }


def usage():
    print(
        "Missing arguments\n"
        + f"\n    -q, --query id            Query a specific schedule"
        + f"\n    -q, --query .             Query all schedules\n"
        + f"\n    -p, --prepare             Prepares a new schedule"
        + f"\n                              Formats must be:"
        + f"\n                                  Time - HH:mm"
        + f"\n                                  Date - dd/MM/yyyy"
        + f"\n    -a, --activate id         Activates a prepared schedule"
        + f"\n    -e, --execute id          Executes an activated schedule"
        + f"\n    -d, --deactivate id       Deactivates an activated schedule"
        + f"\n    -D, --delete id           Deletes a schedule\n"
        + f"\n    -c, --configure           (Re)configures user information"
    )


def createTaskXML(startTime, week_days, sch_id):
    date_time = datetime.datetime.today().strftime('%Y-%m-%dT%H:%M:%S')
    date = datetime.datetime.today().strftime('%Y-%m-%d')
    sufix = startTime.replace(':', '')
    week_maps = {
        "MON": "Monday",
        "TUE": "Tuesday",
        "WED": "Wednesday",
        "THU": "Thursday",
        "FRI": "Friday",
        "SAT": "Saturday",
        "SUN": "Sunday",
    }
    command = f"wmic useraccount where name=\"{os.environ['USERNAME']}\" get sid"
    output = subprocess.check_output(command).decode('utf-8').split('\n')
    sid = output[1].replace('\n', '').replace('\r', '').strip()

    task = Element("Task", {
        "version": "1.2",
        "xmlns": "http://schemas.microsoft.com/windows/2004/02/mit/task"
    })

    registration_info = SubElement(task, "RegistrationInfo")
    SubElement(registration_info, "Date").text = date_time
    author = SubElement(registration_info, "Author")
    author.text = f"{os.environ['COMPUTERNAME']}\{os.environ['USERNAME']}"
    SubElement(registration_info, "URI").text = f"\{sch_id}-{sufix}"

    triggers = SubElement(task, "Triggers")
    calendar_trigger = SubElement(triggers, "CalendarTrigger")
    start_boundary = SubElement(calendar_trigger, "StartBoundary")
    start_boundary.text = f"{date}T{startTime}:00"
    SubElement(calendar_trigger, "Enabled").text = "true"
    schedule_by_week = SubElement(calendar_trigger, "ScheduleByWeek")
    days_of_week = SubElement(schedule_by_week, "DaysOfWeek")
    for week_day in week_days:
        SubElement(days_of_week, week_maps[week_day])
    SubElement(schedule_by_week, "WeeksInterval").text = "1"

    principals = SubElement(task, "Principals")
    principal = SubElement(principals, "Principal", {"id": "Author"})
    SubElement(principal, "UserId").text = sid
    SubElement(principal, "LogonType").text = "Password"
    SubElement(principal, "RunLevel").text = "LeastPrivilege"

    settings = SubElement(task, "Settings")
    SubElement(settings, "MultipleInstancesPolicy").text = "IgnoreNew"
    SubElement(settings, "DisallowStartIfOnBatteries").text = "false"
    SubElement(settings, "StopIfGoingOnBatteries").text = "true"
    SubElement(settings, "AllowHardTerminate").text = "true"
    SubElement(settings, "StartWhenAvailable").text = "false"
    SubElement(settings, "RunOnlyIfNetworkAvailable").text = "false"
    idle_settings = SubElement(settings, "IdleSettings")
    SubElement(idle_settings, "StopOnIdleEnd").text = "true"
    SubElement(idle_settings, "RestartOnIdle").text = "false"
    SubElement(settings, "AllowStartOnDemand").text = "true"
    SubElement(settings, "Enabled").text = "true"
    SubElement(settings, "Hidden").text = "false"
    SubElement(settings, "RunOnlyIfIdle").text = "false"
    SubElement(settings, "WakeToRun").text = "true"
    SubElement(settings, "ExecutionTimeLimit").text = "PT72H"
    SubElement(settings, "Priority").text = "7"

    actions = SubElement(task, "Actions", {"Context": "Author"})
    exec = SubElement(actions, "Exec")
    SubElement(exec, "Command").text = f"\"{sys.executable}\""
    SubElement(exec, "Arguments").text = f"\"{__file__}\" -e {sch_id}"

    rough_string = etree.tostring(task, 'UTF-16')
    reparsed = minidom.parseString(rough_string)

    filename = f"{sch_id}.xml"
    f = open(filename, "w+", encoding="utf-8")
    f.write(reparsed.toprettyxml(indent="  "))
    f.close()
    return filename


if __name__ == "__main__":
    try:
        init()
    except KeyboardInterrupt:
        print("\n\nInterrupted!")
