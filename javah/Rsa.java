package javah;

import java.security.PublicKey;
import java.util.Base64;
import javax.crypto.Cipher;

import java.security.spec.X509EncodedKeySpec;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.nio.charset.StandardCharsets;

public class Rsa {
    public static void main(String[] args) {

        try {
            String public_key_str = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnlc74kbVTrYsok/JH8twLx9WqMb2qWVsPT2Tmi18PCw/4xkPEHtMFF9S67QgAHO+FMj03tn+SdEQU6P0RL0dadKXYyMXZbbz4LWIayot7/qBLLiih6V4UDUBluF6q9SESr258Mgszu4Hzx8eoFS+L1OsgF7JtcErsZWiW5kvc1OHutIwI+7dte/x3DoPkZG0aOJwcyk7admBgapGCqTpiZmG+lCPy9aUWFoXhC8AQAP6SwWi1FUrGoNNa6KFoY68V8vCMEyC5Ea1wLGV6EG5hXsSW6DAvTro+RqgoI6Gya3TDcc2Ep5hHjSOWW2arBeCXfJEWSsUr9Hi/j2KYqN2pwIDAQAB";
            byte[] public_key_byte = Base64.getDecoder().decode(public_key_str);
            X509EncodedKeySpec spec = new X509EncodedKeySpec(public_key_byte);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            RSAPublicKey public_key = (RSAPublicKey) kf.generatePublic(spec);

            String msg = "{'card_number': '5447318955603422', 'card_cvv': '123', 'card_holder_name': 'JOAO DA SILVA', 'card_expiration_date': '0423'}";
            EncriptCard encript = new EncriptCard();
            String result = encript.encript(public_key, msg);
            System.out.println(result);
        } catch (Exception e) {

        }
    }

    public static class EncriptCard {
        public String encript(PublicKey key, String cardNumber) {
            try {
                Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.ENCRYPT_MODE, key);
                byte[] encryptedBytes = cipher.doFinal(cardNumber.getBytes());
                return new String(encryptedBytes, StandardCharsets.UTF_8);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}