#!/usr/bin/python
import os
import re
import shutil

from colorama import init


class Color:
    PURPLE = '\033[95m'
    YELLOW = '\033[93m'
    END = '\033[0m'


def main():
    with open('isprefixes.txt') as isp_file:
        lines = isp_file.readlines()
    scanning = None
    for i, line in enumerate(lines):
        if str(line) == '\n':
            continue
        if not str(line).startswith(' ') or not str(line).startswith(' '):
            scanning = line
            print(Color.YELLOW + "Now scanning ", end='')
            try:
                print(line + Color.END, end='')
            except UnicodeEncodeError:
                print("?" + Color.END, end='')
        else:
            if i % rows == 0:
                print(Color.PURPLE + "Still scanning %s" % scanning + Color.END, end='')
            ip = re.sub(r'/.+', '', line.strip().replace('- ', '').replace('+ ', ''))
            os.system('scan.py -r %s-50 -o "%s"' % (ip, scanning.replace('\n', '')))


if __name__ == "__main__":
    init()
    rows = shutil.get_terminal_size()[1]
    try:
        main()
    except KeyboardInterrupt:
        pass
