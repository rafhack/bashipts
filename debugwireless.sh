#!/bin/bash
while getopts ":i:" o; do
  case "${o}" in
    i)
      devip=${OPTARG}
    ;;
  esac
done

sudo chmod 777 /tmp/adb.log 2>/dev/null
cd ~
adb='adb'

read -p "Connect the device and press Enter "
$adb kill-server
$adb forward tcp:5555 tcp:5555
if [[ $? != 0 ]]; then
  echo "Error forwarding"
  exit 1
fi
$adb tcpip 5555
if [[ $? != 0 ]]; then
  echo "Error enabling tcp mode"
  exit 1
fi
$adb -d connect $devip:5555
if [[ $? != 0 ]]; then
  echo "Error connecting"
  exit 1
fi
echo "Connected"
$adb devices
