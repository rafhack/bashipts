const firePixelsArray = []
const fireWidth = 50
const fireHeight = 50
const debug = false

var firePower = 3
var wind = 0
var htmlString = ""
var numberOfPixels = 0

const originalColors = [{ "r": 7, "g": 7, "b": 7 }, { "r": 31, "g": 7, "b": 7 }, { "r": 47, "g": 15, "b": 7 }, { "r": 71, "g": 15, "b": 7 }, { "r": 87, "g": 23, "b": 7 }, { "r": 103, "g": 31, "b": 7 }, { "r": 119, "g": 31, "b": 7 }, { "r": 143, "g": 39, "b": 7 }, { "r": 159, "g": 47, "b": 7 }, { "r": 175, "g": 63, "b": 7 }, { "r": 191, "g": 71, "b": 7 }, { "r": 199, "g": 71, "b": 7 }, { "r": 223, "g": 79, "b": 7 }, { "r": 223, "g": 87, "b": 7 }, { "r": 223, "g": 87, "b": 7 }, { "r": 215, "g": 95, "b": 7 }, { "r": 215, "g": 95, "b": 7 }, { "r": 215, "g": 103, "b": 15 }, { "r": 207, "g": 111, "b": 15 }, { "r": 207, "g": 119, "b": 15 }, { "r": 207, "g": 127, "b": 15 }, { "r": 207, "g": 135, "b": 23 }, { "r": 199, "g": 135, "b": 23 }, { "r": 199, "g": 143, "b": 23 }, { "r": 199, "g": 151, "b": 31 }, { "r": 191, "g": 159, "b": 31 }, { "r": 191, "g": 159, "b": 31 }, { "r": 191, "g": 167, "b": 39 }, { "r": 191, "g": 167, "b": 39 }, { "r": 191, "g": 175, "b": 47 }, { "r": 183, "g": 175, "b": 47 }, { "r": 183, "g": 183, "b": 47 }, { "r": 183, "g": 183, "b": 55 }, { "r": 207, "g": 207, "b": 111 }, { "r": 223, "g": 223, "b": 159 }, { "r": 239, "g": 239, "b": 199 }, { "r": 255, "g": 255, "b": 255 }]
const fireColorsPalette = [{ "r": 7, "g": 7, "b": 7 }, { "r": 31, "g": 7, "b": 7 }, { "r": 47, "g": 15, "b": 7 }, { "r": 71, "g": 15, "b": 7 }, { "r": 87, "g": 23, "b": 7 }, { "r": 103, "g": 31, "b": 7 }, { "r": 119, "g": 31, "b": 7 }, { "r": 143, "g": 39, "b": 7 }, { "r": 159, "g": 47, "b": 7 }, { "r": 175, "g": 63, "b": 7 }, { "r": 191, "g": 71, "b": 7 }, { "r": 199, "g": 71, "b": 7 }, { "r": 223, "g": 79, "b": 7 }, { "r": 223, "g": 87, "b": 7 }, { "r": 223, "g": 87, "b": 7 }, { "r": 215, "g": 95, "b": 7 }, { "r": 215, "g": 95, "b": 7 }, { "r": 215, "g": 103, "b": 15 }, { "r": 207, "g": 111, "b": 15 }, { "r": 207, "g": 119, "b": 15 }, { "r": 207, "g": 127, "b": 15 }, { "r": 207, "g": 135, "b": 23 }, { "r": 199, "g": 135, "b": 23 }, { "r": 199, "g": 143, "b": 23 }, { "r": 199, "g": 151, "b": 31 }, { "r": 191, "g": 159, "b": 31 }, { "r": 191, "g": 159, "b": 31 }, { "r": 191, "g": 167, "b": 39 }, { "r": 191, "g": 167, "b": 39 }, { "r": 191, "g": 175, "b": 47 }, { "r": 183, "g": 175, "b": 47 }, { "r": 183, "g": 183, "b": 47 }, { "r": 183, "g": 183, "b": 55 }, { "r": 207, "g": 207, "b": 111 }, { "r": 223, "g": 223, "b": 159 }, { "r": 239, "g": 239, "b": 199 }, { "r": 255, "g": 255, "b": 255 }]

function start() {
    createFireDataStruct()
    createFireSource()
    renderFire()
    setInterval(calculateFireProp, 50)
}

function updateFirePixel(pixelIndex) {
    const below = pixelIndex + fireWidth
    if (below >= numberOfPixels) return
    const decay = Math.floor(Math.random() * firePower)
    const newInstensity = firePixelsArray[below] - decay
    firePixelsArray[pixelIndex - (decay + wind)] = newInstensity >= 0 ? newInstensity : 0
}

function createFireDataStruct() {
    numberOfPixels = fireWidth * fireHeight
    for (let i = 0; i < numberOfPixels; i++) {
        firePixelsArray[i] = 0
    }
}

function calculateFireProp() {
    for (let column = 0; column < fireWidth; column++) {
        for (let row = 0; row < fireHeight; row++) {
            updateFirePixel(column + (fireWidth * row))
        }
    }
    renderFire()
}

function renderFire() {
    html('<table cellpadding=0 cellspacing=0>')
    for (let row = 0; row < fireHeight; row++) {
        html('<tr>')
        for (let column = 0; column < fireWidth; column++) {
            const pixelIndex = column + (fireWidth * row)
            const intensity = firePixelsArray[pixelIndex]
            if (debug == true) {
                html('<td class="pixel">')
                html(`<div class="pixel-index">${pixelIndex}</div>`)
                html(intensity)
                html('</td>')
            } else {
                const color = fireColorsPalette[intensity]
                const colorString = `${color.r},${color.g},${color.b}`
                html(`<td class="pixel" style="background-color: rgb(${colorString})"></td>`)
            }
        }
        html('</tr>')
    }
    html('</table>')
    document.querySelector('#fireCanvas').innerHTML = htmlString
    htmlString = ''
}

function createFireSource() {
    for (let column = 0; column <= fireWidth; column++) {
        const pixelIndex = numberOfPixels - fireWidth + column
        firePixelsArray[pixelIndex] = 36
    }
}

function html(content) {
    htmlString += content
}

function updateColor(slider) {
    for (let colorIndex = 0; colorIndex <= 36; colorIndex++) {
        let newColor = 0
        let currentColor = 0
        switch (slider.id) {
            case 'rColorRange':
                currentColor = parseInt(originalColors[colorIndex].r)
                newColor = parseInt(currentColor + parseInt(slider.value))
                fireColorsPalette[colorIndex].r = currentColor <= 7 || newColor >= 255 ? currentColor : newColor
                break;
            case 'gColorRange':
                currentColor = parseInt(originalColors[colorIndex].g)
                newColor = parseInt(currentColor + parseInt(slider.value))
                fireColorsPalette[colorIndex].g = currentColor <= 7 || newColor >= 255 ? currentColor : newColor
                break;
            case 'bColorRange':
                currentColor = parseInt(originalColors[colorIndex].b)
                newColor = parseInt(currentColor + parseInt(slider.value))
                fireColorsPalette[colorIndex].b = currentColor <= 7 || newColor >= 255 ? currentColor : newColor
                break;
        }
    }
}

start()

document.getElementById("rColorRange").oninput = function () { updateColor(this) }
document.getElementById("gColorRange").oninput = function () { updateColor(this) }
document.getElementById("bColorRange").oninput = function () { updateColor(this) }
document.getElementById("windRange").oninput = function () {
    wind = -Math.round(this.value)
}
document.getElementById("fireRange").oninput = function () {
    firePower = 10 - this.value / 10
}