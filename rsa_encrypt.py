import Crypto
from Crypto.PublicKey import RSA
from Crypto import Random
import ast

random_generator = Random.new().read
key = RSA.generate(1024, random_generator) 

publickey = key.publickey()

encrypted = publickey.encrypt('encrypt this message', 32)

print('encrypted message:', encrypted)

f = open('encryption.txt', 'r')
message = f.read()


decrypted = key.decrypt(ast.literal_eval(str(encrypted)))

print 'decrypted', decrypted

f = open('encryption.txt', 'w')
f.write(str(message))
f.write(str(decrypted))
f.close()