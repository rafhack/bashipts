#!/bin/bash

# Check perfect number

echo Checking...

for (( j = 1; j < 10000; j++ )); do

  result=0;
  for (( i = 1; i <= $j; i++ )); do
    if [[ $(( $j % $i )) == 0 ]]; then
      result=$(($result + $i))
    fi
  done

  result=$(($result / 2))

  if [[ $j == $result ]]; then
    echo $j is a perfect
  fi
done
