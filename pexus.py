#!/usr/bin/python
""" Pexus - Make a request to Riot Current Game API """
import json
import requests
import sys
from colorama import init
from pathlib import Path
from threading import Thread


class Color:
    """ Color enumeration class """

    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


class Consts:
    """ Class containing default constant values """

    BASE_URL = "https://br1.api.riotgames.com/"
    API_KEY = "69c55789-4806-48e9-8c8a-15e0c2e74b04"


def main():
    """ Main function """
    global summoner_id
    sum_id_path = Path('summoner_id')
    if sum_id_path.is_file() and '--clear' not in sys.argv:
        with open('summoner_id') as sum_id_file:
            summoner_id = sum_id_file.read()
    else:
        summoner_id = get_summoner_id(input('Summoner name: '))
    path = (['BR1'], [summoner_id])
    route = "observer-mode/rest/consumer/getSpectatorGameInfo"
    response = invoke_get(route, path_params=path)
    showdata(response)


def showdata(response):
    data = json.loads(response)
    if 'status' in data:
        print('Not playing')
    else:
        participants = data['participants']
        threads = []
        red_team_results = []
        blue_team_results = []
        for part in participants:
            threads.append(Thread(target=player_info, args=([part, red_team_results, blue_team_results])))
        list(map(lambda x : x.start(), threads))
        list(map(lambda x : x.join(), threads))
        list(map(lambda x : print(Color.RED + x), blue_team_results))
        list(map(lambda x : print(Color.GREEN + x), red_team_results))


def player_info(part, red, blue):
    champ = get_champ_name(part['championId'])
    spell1 = get_spell_name(part['spell1Id']) + " - "
    spell2 = get_spell_name(part['spell2Id'])
    elo = get_elo(str(part['summonerId']))

    if part['teamId'] == 100:
        try:
            red.append(part['summonerName'] + " (" + champ + ") [" + spell1 + spell2 + "] - " + elo)
        except TypeError:
            pass
    else:
        try:
            blue.append(part['summonerName'] + " (" + champ + ") [" + spell1 + spell2 + "] - " + elo)
        except TypeError:
            pass


def get_elo(id):
    route = "api/lol/br/v2.5/league/by-summoner"
    path = ([id], ['entry'])
    response = invoke_get(route, path_params=path)
    data = json.loads(response)
    if 'status' in data:
        return "Unranked"
    for entrie in data:
        tier = data[entrie][0]['tier']
        div = data[entrie][0]['entries'][0]['division']
        return tier + " " + div


def get_summoner_id(name):
    route = 'api/lol/br/v1.4/summoner/by-name'
    path = [[name]]
    response = invoke_get(route, path_params=path)
    data = json.loads(response)
    if 'status' in data:
        print('Not found')
        sys.exit(1)
    for found_name in data:
        id = str(data[found_name]['id'])
        with open('summoner_id', 'w') as file_id:
            file_id.write(id)
        return str(data[found_name]['id'])


def get_spell_name(id):
    spell_path = Path('spells.json')
    if spell_path.is_file():
        with open('spells.json') as spell_file:
            data = json.loads(spell_file.read())
    else:
        route = "api/lol/static-data/na/v1.2/summoner-spell"
        response = invoke_get(route)
        with open('spells.json', 'w+') as spell_file:
            spell_file.write(response)
        data = json.loads(response)

    spells = data['data']
    for spell in spells:
        sp_info = data['data'][spell]
        if sp_info['id'] == id:
            return sp_info['name']


def get_champ_name(id):
    champ_path = Path('champs.json')
    if champ_path.is_file():
        with open('champs.json', 'r') as champ_file:
            data = json.loads(champ_file.read())
    else:
        route = "api/lol/static-data/na/v1.2/champion"
        response = invoke_get(route)
        with open('champs.json', 'w+') as champ_file:
            champ_file.write(response)
        data = json.loads(response)
    champs = data['data']
    for champ in champs:
        ch_info = data['data'][champ]
        if ch_info['id'] == id:
            return ch_info['name']


def query_maker(key, value):
    """ Creates a query parameter """
    return [(key, value)]


def invoke_get(route, path_params=(), query_params=()):
    """ h """
    paths = ''
    queries = ''

    if path_params:
        for param in path_params:
            if len(param) > 1:
                paths += "/" + param[0] + "/" + param[1]
            else:
                paths += "/" + param[0]
    if query_params:
        for param in query_params:
            queries += "?" + param[0] + "=" + param[1]

    queries += '?api_key=' + Consts.API_KEY

    resp = requests.get(Consts.BASE_URL + route + paths + queries)
    return resp.text

if __name__ == "__main__":
    init()
    try:
        main()
        input()
    except KeyboardInterrupt:
        sys.exit(2)
