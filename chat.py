#!/usr/bin/python
""" Simple chat in python """

import socket
import sys
import time
import os
from colorama import init
from threading import Thread


class Color:
    """ Color enumeration class """

    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def main():
    """ main function, initializes menus """

    main_opt = mainmenu()
    if main_opt == 1:
        create_room()
        enter_room(room=get_local_addr())
    elif main_opt == 2:
        enter_room()


def enter_room(room=''):
    """ Enters an existing room """

    set_nickname()
    room_addr = (enter_room_menu() if not room else room, 8888)
    sock = socket.socket()
    sock.connect(room_addr)
    sock.send(NICKNAME.encode())
    recv = Thread(target=listen_messages, args=([sock]))
    recv.setDaemon(True)
    recv.start()
    time.sleep(1)
    write_messages(sock)


def write_messages(sock):
    """ Thread for writing messages """
    while True:
        msg = input(NICKNAME + ": ")
        if msg:
            msg = "\n" + Color.GREEN + NICKNAME + ": " + Color.END + msg
            sock.send(msg.encode("utf-8"))


def listen_messages(sock):
    """ Listens for messages from the server """
    while True:
        try:
            msg = sock.recv(1024)
        except Exception:
            print(Color.RED + Color.BOLD + "Server has gone offline" + Color.END)
            os._exit(1)
        if msg:
            str_msg = msg.decode()
            if "[SERVER]" in str_msg:
                print(Color.BOLD + Color.RED)
            try:
                print(chr(27) + '[2A' + "\n" + msg.decode() + Color.END)
            except UnicodeEncodeError:
                print(Color.RED + Color.BOLD + "[ Encoding error :( ]" + Color.END)
            if "[SERVER]" not in str_msg:
                print(NICKNAME + ": ", end='')


def server_listen_messages(sock, clients):
    """ Listens for messages from clients """
    while True:
        try:
            msg = sock.recv(1024)
        except Exception as e:
            print(e)
            break
        if msg:
            for client in clients:
                if client[0].getpeername()[0] != sock.getpeername()[0]:
                    client[0].sendall(msg)


def set_nickname():
    """ Sets a nickname for a session """

    global NICKNAME
    NICKNAME = ''
    print(Color.BOLD + Color.YELLOW + "Enter your nickname: " + Color.END, end='')
    while not NICKNAME:
        NICKNAME = input()


def enter_room_menu():
    """ Shows the menu for "enter room" screen """

    print(Color.BOLD + Color.GREEN + "Enter room address: " + Color.END, end='')
    return input()


def create_room():
    """ Creates a new room socket """

    sock = socket.socket()
    local_addr = (get_local_addr(), 8888)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(local_addr)
    sock.listen()
    thread = Thread(target=start_daemon, args=([sock]))
    thread.setDaemon(True)
    thread.start()


def start_daemon(sock):
    """ Start listening for connections """
    clients = []
    while True:
        conn, addr = sock.accept()
        clients.append((conn, addr))
        nick = conn.recv(1024).decode("utf-8")
        message = "[SERVER] Hey there " + nick + "! This is the room " + get_local_addr() + "\n"
        join_msg = "[SERVER] " + nick + " joined the room\n"
        conn.sendto(message.encode(), clients[len(clients) - 1][1])
        for client in clients:
            try:
                client[0].sendall(join_msg.encode("utf-8"))
            except ConnectionResetError:
                pass
            client_thread = Thread(target=server_listen_messages, args=([conn, clients]))
            client_thread.setDaemon(True)
            client_thread.start()


def get_local_addr():
    """ Gets LAN address """
    sock = socket.socket()
    sock.connect(("www.google.com", 80))
    local_ip = sock.getsockname()[0]
    sock.close()
    return local_ip


def mainmenu():
    """ Shows the first menu on script """

    print(Color.BOLD + Color.GREEN + "[1] - Create a room")
    print("[2] - Enter existnig room" + Color.END)
    return menu_creator(1, 2)


def menu_creator(*options, prompt="->"):
    """ create default menus """
    choice = ''
    while not choice or (int(choice) not in options):
        choice = input(prompt + " ")
    return int(choice)


if __name__ == "__main__":
    try:
        init()
        main()
    except KeyboardInterrupt:
        print(Color.RED + Color.BOLD + "Program interrupted" + Color.END)
        sys.exit(0)
    except ValueError:
        print(Color.RED + Color.BOLD + "Enter a valid option" + Color.END)
        sys.exit(1)
