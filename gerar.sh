#!/bin/bash

function push {
    echo "-----------------------------------------"
    git log origin/$branch...$branch
    echo "-----------------------------------------"
    echo "[?] Do you want to push these commited changes? [Y/N]"
    read push
    if [[ ${push,,} == "y" ]]; then
      echo "[+] Pushing commits"
      git push origin $branch
      if [[ $? == 0 ]]; then
        echo "[+] Push successful!"
      else
        echo "[X] Push failed, aborting."
        exit 1
      fi
    fi
}

function checkVersion {
	echo Checking versions...

	VERSION_TMP=$(grep "versionName" app/build.gradle | awk '{print $2}')
	VERSION=$(echo $VERSION_TMP | sed -e 's/^"//'  -e 's/"$//' |  tr -d '. ')
	local_vname=$(echo $VERSION_TMP | sed -e 's/^"//'  -e 's/"$//')

	svjson=$(wget -qO- $GET_VERSION_URL)
	svvs=$(echo $svjson | jshon -e 0 -e "Version" | sed -e 's/^"//' -e 's/"$//' |  tr -d '.')
	svvsname=$(echo $svjson | jshon -e 0 -e "Version" | sed -e 's/^"//' -e 's/"$//')

	if [[ (( "$svvs" -ge "$VERSION" )) ]]; then
		echo Update not needed
		echo "Local version:  ${bold}$local_vname${normal}"
		echo "Server version: ${bold}$svvsname${normal}"
		exit 0
	fi
}

function commit {
	echo "[!] There are uncommited changes, do you want to commit them? [Y/N]"
	read commit
	if [[ ${chg,,} == "y" ]]; then
		echo "[+] Commiting changes..."
		git add app/.
		git commit -m "Auto generated version"
		if [[ $? == 0 ]]; then
			echo "[+] Commit successful!"
		else
			echo "[X] Commit failed, aborting."
			exit 1
		fi
	fi
}

function updateVersion {
	echo "[+] Updating file..."
	filename="${apk##*/}"
	if [[ -f $filename ]]; then
		rm $DROPBOX_DIR/its4/prod/app-producao-debug.apk
	fi
	cp -f $apk $DROPBOX_DIR/its4/prod/app-producao-debug.apk 2>/dev/null

	echo "[+] Uploading file..."
	while [[ $(python2 $SCRIPTS_DIR/dropbox.py status) != "Up to date" ]]; do sleep 0s; done
	echo "[+] Recording version $local_vname on server..."

	json='
	{
	"Version": "'"$local_vname"'",
	"Url": "https://www.dropbox.com/s/v5rsnpb3986rys3/app-producao-debug.apk?dl=1",
	"Description": "First release tablet",
	"TypeDeply": 4
	}
	'

	echo $SCRIPTS_DIR
	echo $local_vname
	read

	wget -qO- --post-data="$json" --header "Content-Type: application/json" $POST_VERSION_URL
	echo "[+] Done"
}

bold=$(tput bold)
normal=$(tput sgr0)
bashDir=$(dirname $0)
localdir=$(pwd)

. $bashDir/gerar_config.sh
cd $STUDIO_PROJS_DIR/its4-Android

checkVersion

chg=$(git status -s | grep -v  "modules\|gradlew\|inspection\|captures");
branch=$(git name-rev --name-only HEAD);
nogit=$1

if [[ $chg != "" && ${nogit,,} != "--nogit" ]]; then
	echo "-----------------Changes-----------------"
	git status -s app
	echo "-----------------------------------------"
	commit
fi

commits=$(git log origin/$branch...$branch)

if [[ $commits != "" && ${nogit,,} != "--nogit" ]]; then
  push
fi

cd ~/scripts
. ./build.sh -p its4-Android -m producao -i noinstall
echo "[?] Ready! Do you really want to proceed? [Y/N]"
read generate
if [[ ${generate,,} == "y" ]]; then
	updateVersion
fi

cd $localdir