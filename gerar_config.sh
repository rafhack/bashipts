#!/bin/bash

STUDIO_PROJS_DIR=~/StudioProjects
DROPBOX_DIR=~/Dropbox
SCRIPTS_DIR=~/scripts

#Version urls
GET_VERSION_URL="https://its4schoolsapi.azurewebsites.net/version/environment/4"
POST_VERSION_URL="https://its4schoolsapi.azurewebsites.net/version"