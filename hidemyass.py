#!/usr/bin/python
from requests.exceptions import *
import requests
import json
import sys

def invoke_get():
    """ h """
    url = "http://gimmeproxy.com/api/getProxy?get=true&maxCheckPeriod=3600"
    resp = requests.get(url, proxies=proxies if proxies else {}, timeout=5)
    return resp.text

def get_proxy():
    resp = invoke_get()
    data = json.loads(resp)
    return data['ipPort']

if __name__ == "__main__":
    global proxies
    proxies = {'http': '207.148.65.57:80'}
    prx = []
    for i in range(1, 50):
        try:
            try:
                proxy = get_proxy()
            except (ProxyError, ConnectionError, ConnectTimeout, ReadTimeout, ConnectionResetError, ValueError) as e:
                proxies = {'http': '207.148.65.57:80'}
                try:
                    proxy = get_proxy()
                except Exception:
                    print("Try again later")
                    break
            proxies = {'http': 'http://{0}:{1}'.format(proxy.split(':')[0], proxy.split(':')[1])}
            if "http://" + proxy + "\n" not in prx:
                print("Get brand new proxies... {0} of {1}".format(i, 50))
                print(proxy)
                prx.append("http://" + proxy + "\n")
        except KeyboardInterrupt:
            break
    with open("proxy_list", mode='w') as f:
        f.writelines(prx)
        f.close()
