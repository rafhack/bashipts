#!/usr/bin/python3
""" Stitch Automator """

import datetime
import pytz
import getopt
import json
import os
import subprocess
import sys
import time
import requests
import urllib3
import base64
import traceback

from typing import Dict
from random import random

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials


SUB_KEY = "Software\HNDSofts\Stitch"


def init():
    parse_params()


def parse_params():
    longopts = ["query", "prepare", "execute=",
                "deactivate" "activate", "configure", "delete"]
    shoropts = "qpe:dacD"
    try:
        opts, _ = getopt.getopt(sys.argv[1:], shoropts, longopts)
        for opt, arg in opts:
            if opt in ('-q', '--query'):
                query_schedule()
            if opt in ('-p', '--prepare'):
                prepare_cron_schedule()
            if opt in ('-a', '--activate'):
                activate_stitch()
            if opt in ('-e', '--execute'):
                execute_stitch(arg)
            if opt in ('-c', '--configure'):
                configure_auth()
            if opt in ('-D', '--delete'):
                delete_schedule()
            if opt in ('-d', '--deactivate'):
                deactivate_schedule()
        if not opts:
            usage()
    except getopt.GetoptError as e:
        print(e)
        usage()


def delete_schedule():
    confirm = input("Delete? [Y/N] ")
    if confirm.lower() != 'y':
        return
    config = get_configuration()
    config.update({
        "crons": [],
        "fromDayOfWeek": "",
        "startTimes": [],
        "toDayOfWeek": ""
    })
    save_configuration(config)
    delete_crons()


def deactivate_schedule():
    confirm = input("Deactivate? [Y/N] ")
    if confirm.lower() == 'y':
        delete_crons()


def delete_crons():
    os.system("crontab -l | grep -v \"#stitch\" | crontab -")


def configure_auth():
    try:
        c = get_configuration()

        print("\n==========Tangerino Settings==========\n")
        employer = input(f"Tangerino Employer [{c.get('employer', '')}]: ")
        pin = input(f"Tangerino PIN [{c.get('pin', '')}]: ")
        jSessionId = input(f"Tangerino JSESSIONID [{c.get('jSessionId', '')}]: ")
        userId = input(f"Tangerino User ID [{c.get('userId', '')}]: ")
        authorization = input(f"Tangerino Authorization [{c.get('authorization', '')}]: ")
        dayStartMinDelay = input(f"Day start min delay (minutes) [{c.get('dayStartMinDelay', '')}]: ")
        dayStartMaxDelay = input(f"Day start max delay (minutes) [{c.get('dayStartMaxDelay', '')}]: ")
        print("\n==========Clockify Settings==========\n")
        clockifyApiToken = input(f"Clockify API Token [{c.get('clockifyApiToken', '')}]: ")
        clockifyProjectId = input(f"Clockify Porject ID [{c.get('clockifyProjectId', '')}]: ")
        print("\n==========Jira Settings==========\n")
        jiraApiToken = input(f"Jira API Token [{c.get('jiraApiToken', '')}]: ")
        jiraUserEmail = input(f"Jira User Email [{c.get('jiraUserEmail', '')}]: ")
        print("\n==========Calendar Settings==========\n")

        stichyConfiguration = {
            "employer": employer if employer else c.get('employer', ''),
            "pin": pin if pin else c.get('pin', ''),
            "jSessionId": jSessionId if jSessionId else c.get('jSessionId', ''),
            "userId": userId if userId else c.get('userId', ''),
            "authorization": authorization if authorization else c.get('authorization', ''),
            "clockifyApiToken": clockifyApiToken if clockifyApiToken else c.get('clockifyApiToken', ''),
            "dayStartMinDelay": dayStartMinDelay if dayStartMinDelay else c.get('dayStartMinDelay', ''),
            "dayStartMaxDelay": dayStartMaxDelay if dayStartMaxDelay else c.get('dayStartMaxDelay', ''),
            "clockifyProjectId": clockifyProjectId if clockifyProjectId else c.get('clockifyProjectId', ''),
            "jiraApiToken": jiraApiToken if jiraApiToken else c.get('jiraApiToken', ''),
            "jiraUserEmail": jiraUserEmail if jiraUserEmail else c.get('jiraUserEmail', ''),
            "installed": {
                "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                "client_id": "667044283558-nanfnjjmluv7dnj642kdq6qlicfpab5v.apps.googleusercontent.com",
                "client_secret": "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",
                "project_id": "stitchy-png",
                "redirect_uris": [
                    "urn:ietf:wg:oauth:2.0:oob",
                    "http://localhost"
                ],
                "token_uri": "https://oauth2.googleapis.com/token"
            }
        }

        c.update(stichyConfiguration)
        save_configuration(c)

        events = get_calendar_events(None)
        for i, event in enumerate(events):
            print(f'[{i}] {event["summary"]}')

        prompt = "\nSelect your daily event: "
        choice = input(prompt)
        while not choice:
            choice = input(prompt)

        c = get_configuration()
        c.update({"iCalUID": events[int(choice)]["iCalUID"]})
        save_configuration(c)

        print("\nNext step: Prepare your schedule by running with \"-p\" flag")

    except OSError as e:
        print(e)


def execute_stitch(arg):
    try:
        configuration = get_configuration()

        date = datetime.datetime.today().strftime('%d/%m/%Y')
        if (date in configuration["skipDates"]):
            print("Skip date, exiting!")
            return

        manage_clockify(int(arg))

        # Humanize stitch
        wait_time = get_random_minute(maximun_wait=3)
        time.sleep(wait_time)

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        employer = configuration["employer"]
        pin = configuration["pin"]
        user_id = configuration["userId"]
        session_id = configuration["jSessionId"]
        auth = configuration["authorization"]

        baseURL = "https://app.tangerino.com.br/Tangerino/ws"
        headers = {
            "EMPREGADOR": employer,
            "pin": pin,
            "Cookie": "JSESSIONID={}".format(session_id),
            "funcionarioid": user_id,
            "USERNAME": user_id,
            "authorization": auth,
            "Content-Type": "application/json"
        }

        requests.get("{}/fingerprintWS/funcionario/empregador/{}/pin/{}"
                           .format(baseURL, employer, pin), verify=False, headers=headers)

        body = {'deviceId': None}
        requests.post("{}/autorizaDipositivoWS/verifica/web/empregador/{}/pin/{}"
                            .format(baseURL, employer, pin), verify=False, headers=headers, data=json.dumps(body))

        date_time = datetime.datetime.today().strftime('%d/%m/%Y %H:%M:%S')
        body = {
            "horaInicio": date_time,
            "deviceId": None,
            "online": "true",
            "codigoEmpregador": employer,
            "pin": pin,
            "horaFim": "",
            "tipo": "WEB",
            "foto": "",
            "intervalo": "",
            "validFingerprint": False,
            "versao": "registra-ponto-fingerprint",
            "plataforma": "WEB",
            "funcionarioid": user_id,
            "idAtividade": 6,
            "latitude": None,
            "longitude": None
        }
        requests.post("{}/pontoWS/ponto/sincronizacaoPontos/1.2"
                            .format(baseURL), verify=False, headers=headers, data=json.dumps(body))

    except Exception:
        f = open("stitchy.log", "a")
        f.write(datetime.datetime.today().strftime('\n%d/%m/%Y %H:%M:%S\n'))
        f.write(traceback.format_exc())
        f.close()

def activate_stitch():
    try:
        deactivate_schedule()
        configuration = get_configuration()

        confirm = input("Current schedule: {}\n\nActivate schedule? [Y/N] "
                        .format(configuration))

        if confirm.lower() != 'y':
            return

        for i, cron in enumerate(configuration["crons"]):
            command = f"(crontab -l 2>/dev/null; echo \"{cron} {sys.executable} {f'{os.getcwd()}/{sys.argv[0]}'} -e {i} #stitch\") | crontab -"
            try:
                os.system(command)
            except subprocess.CalledProcessError:
                print("Failed to create schedule")
                return

    except OSError as e:
        print("Could not find schedule with the specified id")
        print(e)


def prepare_cron_schedule():
    start_times = ["09:00", "12:00", "13:00", "18:00"]
    input_times = input(
        "Start Times (Separated by colons, like: \"{}\"): ".format(start_times).replace("[", "").replace("'", "").replace("]", ""))
    start_times = input_times.split(',') if input_times else start_times

    from_day_of_week = input("From day of week (Ex: mon or fri): ")
    from_day_of_week = from_day_of_week if from_day_of_week else "mon"

    to_day_of_week = input("To day of week: ")
    to_day_of_week = to_day_of_week if to_day_of_week else "fri"

    skip_dates = []
    input_skip_dates = input(
        "Skip dates (dd/MM/yyyy): (Separated by colons): {} ".format(skip_dates))
    skip_dates = input_skip_dates.split(',') if input_skip_dates else skip_dates

    crons = []
    for time in start_times:
        time_units = time.split(":")
        crons.append(
            f"{time_units[1]} {time_units[0]} * * {from_day_of_week.upper()}-{to_day_of_week.upper()}")

    try:
        readConfiguration = get_configuration()
        readConfiguration.update({
            "startTimes": start_times,
            "fromDayOfWeek": from_day_of_week,
            "toDayOfWeek": to_day_of_week,
            "crons": crons,
            "skipDates": skip_dates
        })

        save_configuration(readConfiguration)

    except OSError as e:
        print("Missing configurations. Use -c option")
        print(e)

    query_schedule()


def save_configuration(configuration: Dict):
    dir = os.path.dirname(os.path.realpath(__file__))
    wf = open(f"{dir}/stichy-pgn.json", "w")
    wf.write(json.dumps(configuration, sort_keys=True, indent=4))
    wf.close()


def get_configuration():
    dir = os.path.dirname(os.path.realpath(__file__))
    rf = open(f"{dir}/stichy-pgn.json", "r")
    configutarionFile = rf.read()
    readConfiguration: Dict = json.loads(configutarionFile)
    rf.close()
    return readConfiguration


def query_schedule():
    try:
        print(json.dumps(get_configuration()["crons"], indent=4))
    except OSError as e:
        print(e)


def usage():
    print(
        "Missing arguments\n"
        + f"\n    -q, --query .             Query all schedules\n"
        + f"\n    -p, --prepare             Prepares a new schedule"
        + f"\n                              Formats must be:"
        + f"\n                                  Time - HH:mm"
        + f"\n                                  Date - dd/MM/yyyy"
        + f"\n    -a, --activate id         Activates a prepared schedule"
        + f"\n    -e, --execute index      Executes an activated schedule with action index"
        + f"\n    -d, --deactivate id       Deactivates an activated schedule"
        + f"\n    -D, --delete id           Deletes a schedule\n"
        + f"\n    -c, --configure           (Re)configures user information"
    )


def get_random_number(min, max):
    return round(random() * (max - min) + min)


def get_random_minute(minimun_wait=0, maximun_wait=10):
    return get_random_number(minimun_wait, maximun_wait) * 60


def start_clockify(date_time=datetime.datetime.today().utcnow()):
    try:
        config = get_configuration()

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        baseURL = "https://api.clockify.me/api/v1"
        headers = {
            "X-Api-Key": config["clockifyApiToken"],
            "Content-Type": "application/json"
        }

        workspaces_response = requests.get(
            f"{baseURL}/workspaces", verify=False, headers=headers)
        workspace_id = json.loads(workspaces_response.text)[0]["id"]

        date_time = date_time.strftime('%Y-%m-%dT%H:%M:%S.000Z')

        body = {
            "start": date_time,
            "billable": False,
            "description": "",
            "projectId": config["clockifyProjectId"],
            "taskId": None,
            "tagIds": None,
            "customFields": []
        }
        requests.post(f"{baseURL}/workspaces/{workspace_id}/time-entries",
                      verify=False, headers=headers, data=json.dumps(body))
    except Exception as e:
        print(e)


def stop_clockify(description=None, end_date_time=datetime.datetime.today().utcnow()):
    try:
        config = get_configuration()

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        baseURL = "https://api.clockify.me/api/v1"
        headers = {
            "X-Api-Key": config["clockifyApiToken"],
            "Content-Type": "application/json"
        }

        user_response = requests.get(
            f"{baseURL}/user", verify=False, headers=headers)
        userId = json.loads(user_response.text)["id"]

        workspaces_response = requests.get(
            f"{baseURL}/workspaces", verify=False, headers=headers)
        workspaceId = json.loads(workspaces_response.text)[0]["id"]

        date_time = end_date_time.strftime('%Y-%m-%dT%H:%M:%S.000Z')

        params = {"page-size": 1}
        time_entries_reponse = requests.get(
            f"{baseURL}/workspaces/{workspaceId}/user/{userId}/time-entries", headers=headers, params=params)
        time_entry = json.loads(time_entries_reponse.content)[0]
        time_entry_id = time_entry["id"]

        time_entry.update({
            "end": date_time,
            "start": time_entry["timeInterval"]["start"],
            "description": description if description else get_current_jira_task()
        })

        requests.put(f"{baseURL}/workspaces/{workspaceId}/time-entries/{time_entry_id}",
                     verify=False, headers=headers, data=json.dumps(time_entry))

        return time_entry["description"]
    except Exception:
        return ""


def get_current_jira_task():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    config = get_configuration()
    jira_token = config["jiraApiToken"]
    jira_email = config["jiraUserEmail"]
    authorization = base64.b64encode(
        bytes(f"{jira_email}:{jira_token}", "utf-8"))
    headers = {
        "Authorization": f"Basic {authorization.decode()}",
        "Content-Type": "application/json"
    }
    base_url = "https://acctglobal.atlassian.net/rest/api/3"

    params = {
        "query": [jira_email]
    }

    response = json.loads(requests.get(
        f"{base_url}/groupuserpicker", verify=False, headers=headers, params=params).text)
    account_id = response["users"]["users"][0]["accountId"]

    params = {
        "jql": f'status in ("In progress") and assignee in ("{account_id}") order by status'
    }
    response = json.loads(requests.get(
        f"{base_url}/search", verify=False, headers=headers, params=params).text)
    try:
        return response["issues"][0]["fields"]["summary"]
    except:
        return "Bug fixes"


def get_calendar_events(iCalUID=get_configuration()["iCalUID"]):
    creds = get_google_credentials()
    service = build('calendar', 'v3', credentials=creds)

    now = datetime.datetime.utcnow()
    past_midnight = now - datetime.timedelta(hours=now.hour,
                                             minutes=now.minute, seconds=now.second)
    next_midnight = past_midnight + datetime.timedelta(days=1)
    events_result = service.events().list(calendarId='primary', timeMin=past_midnight.isoformat() + 'Z',
                                          timeMax=next_midnight.isoformat() + 'Z', maxResults=30, singleEvents=True,
                                          orderBy='startTime', iCalUID=iCalUID).execute()
    return events_result.get('items', [])


def get_daily_event_start_time():
    event = get_calendar_events()[0]
    start = event['start'].get('dateTime', event['start'].get('date'))

    return datetime.datetime.strptime(start, "%Y-%m-%dT%H:%M:%S%z")


def get_google_credentials():
    config = get_configuration()
    scopes = ['https://www.googleapis.com/auth/calendar.readonly']

    dir = os.path.dirname(os.path.realpath(__file__))
    config_file = f"{dir}/stichy-pgn.json"

    try:
        creds = Credentials.from_authorized_user_file(config_file, scopes)
        creds.refresh(Request())
    except:
        flow = InstalledAppFlow.from_client_secrets_file(config_file, scopes)
        flow.redirect_uri = flow._OOB_REDIRECT_URI
        code = input(f'{flow.authorization_url()[0]}\n')
        flow.fetch_token(code = code)
        creds = flow.credentials
        config.update(json.loads(creds.to_json()))
        save_configuration(config)
        creds = Credentials.from_authorized_user_file(config_file, scopes)

    return creds


def manage_clockify(arg):
    config = get_configuration()
    if arg == 0:
        date_time = datetime.datetime.today().utcnow()
        
        date_time = date_time + \
            datetime.timedelta(minutes=get_random_number(
                int(config["dayStartMinDelay"]), int(config["dayStartMaxDelay"])))
        start_clockify(date_time)
    elif arg == 1:
        daily_time = get_daily_event_start_time().astimezone(pytz.utc)
        running_task_name = stop_clockify(end_date_time=daily_time)
        start_clockify(daily_time)
        end_daily_time = daily_time + \
            datetime.timedelta(minutes=get_random_number(15, 20))
        stop_clockify("Daily", end_daily_time)
        start_clockify(end_daily_time)
        stop_clockify(running_task_name)
    elif arg == 2:
        start_clockify()
    elif arg == 3:
        stop_clockify()


if __name__ == "__main__":
    try:
        init()
    except KeyboardInterrupt:
        print("\n\nInterrupted!")
