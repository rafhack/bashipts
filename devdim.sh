#!/bin/bash
if [[ $1 == 'reset' ]]; then
	adb shell wm size reset
	adb shell wm density reset
	exit 0
fi
adb shell wm size 1920x1080
adb shell wm density 423
