import requests
import urllib.parse
import json
from bs4 import BeautifulSoup
import webbrowser


def get_proxies(WebUrl):
    url = WebUrl
    code = requests.get(url)
    plain = code.text
    proxies = []
    s = BeautifulSoup(plain, "html.parser")
    proxy_table = s.find('table', {'id': 'proxylisttable'})
    for link in proxy_table.findAll('tr'):
        proxy_data = link.findAll('td')
        array = []
        for td in proxy_data:
            array.append(str(td.contents[0]))
        if len(array) == 8:
            if array[6] == 'yes':
                proxies.append("{}:{}".format(array[0], array[1]))
    return proxies


def init():
    url = input("URL: ")
    proxies = get_proxies("https://free-proxy-list.net")
    for proxy in proxies:
        link = get_link(proxy.split(':'), url)
        if link != None:
            print('{} <----'.format(link))
            webbrowser.open_new_tab(link)
            break


def get_link(proxy, url):
    proxies = {'https': '{}:{}'.format(proxy[0], proxy[1].replace('\n', ''))}
    body = 'upl={}&password=&fg=false'.format(urllib.parse.quote(url, safe=''))
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'User-Agent': 'Mozilla'
    }
    try:
        print("Proxy: {}".format(proxies))
        response = requests.post("https://www.uploadedpremiumlink.net/generate", proxies=proxies, data=body, headers=headers, timeout=10)
        if response.status_code == 200:
            response = json.loads(response.content)
            print('{} - {}'.format(response['pname'], response['psize']))
            return response['adsLink']
        else:
            print(response.status_code)
    except KeyboardInterrupt:
        exit(0)
    except:
        return None


if __name__ == "__main__":
    init()
